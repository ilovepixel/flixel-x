#include <flixel/FlxGame.h>
#include <flixel/FlxG.h>
#include <flixel/FlxSprite.h>

struct DemoState : public FlxState
{
	FlxSprite someSprite;
	FlxSprite spaceman;
	FlxSprite sample;
	FlxGroup group;
	float32 move;
	DemoState() :
		move(0),
		someSprite(100, 100)
	{}
	void create()
	{		
		spaceman.loadGraphic(FlxImage::loadImage("data/spaceman.png", FlxImage::PNG), true, true, 16, 16);
		sample.makeGraphic(300, 50, FlxG::RED);
		sample.x = 10;
		sample.y = 200;
		FlxArray<uint32> walkFrames(4);
		walkFrames.push(1);
		walkFrames.push(2);
		walkFrames.push(3);
		walkFrames.push(0);
		spaceman.addAnimation("walk", walkFrames, 12);
		//spaceman.velocity.x = 100;
		spaceman.play("walk");
		spaceman.x = 150;
		spaceman.y = 150;
		spaceman.acceleration.y = 20;
		someSprite.acceleration.y = 20;
		group.add(&someSprite);
		group.add(&spaceman);
		//group.add(&sample);
		FlxG::shake(0.01f);
		FlxG::flash();	
		//FlxG::playMusic(FlxAudio::loadAudio("data/music1.wav"));
		for (size_t i = 0; i < 100; ++i) 
		{
			FlxSprite* s = new FlxSprite(FlxG::random() * 310, FlxG::random() * 100);
			s->addAnimation("walk", walkFrames, 12);
			s->loadGraphic(spaceman.getPixels(), true, true, 16, 16);
			s->acceleration.y = 20;
			s->elasticity = 0.5f;
			group.add(s);
		}
		add(&group);
		add(&sample);
		sample.immovable = true;
	}
	void update()
	{
		FlxState::update();
		FlxG::collide(&group, &sample);
		FlxG::collide(&group, &group);
		float32 scale = 1.0f + fabs(cosf(someSprite.angle / 50)* 4.0f);
		//someSprite.angle += 1;
		//someSprite.scale.make(scale, scale);
		if (FlxG::keyboard.isKeyHit(FlxKeyboard::KEY_SPACE))
		{
			sample.angle += 1;
			//FlxG::shake(0.01f);
			//FlxG::flash();

		}
		if (spaceman.x > FlxG::width - 100 | spaceman.x < 100)
		{
			spaceman.velocity.x *= -1;
			spaceman.scale.x *= -1;
		}
		sample.velocity.y = sinf(move) * 200;

		move += 0.05f;
	}
};






int main(int argc, const char* argv[])
{

	new FlxGame(800, 480, GetClass<DemoState>());

	return 0;
}