#include <flixel/FlxG.h>
#include <flixel/FlxObject.h>
#include <float.h>
#include <flixel/system/FlxQuadTree.h>

bool FlxG::paused = false;
bool FlxG::debug = false;
float32 FlxG::elapsed = 0;
float32 FlxG::timeScale = 0;
float32 FlxG::width = 0;
float32 FlxG::height = 0;
FlxRect FlxG::worldBounds;
uint32 FlxG::worldDivisions = 0;
bool FlxG::visualDebug = false;
bool FlxG::mobile = false;
int32 FlxG::globalSeed = 0;
uint32 FlxG::save = 0;
FlxGroup FlxG::sounds(100);
bool FlxG::mute = false;
FlxStaticArray<FlxCamera*, 100> FlxG::cameras;
FlxCamera* FlxG::camera = nullptr;
FlxCanvas2D* FlxG::canvas = nullptr;
FlxAudioPlayer* FlxG::audioPlayer = nullptr;
float32 FlxG::_volume = 1.0f;
FlxGame* FlxG::_game = nullptr;
FlxKeyboard FlxG::keyboard;
FlxMouse FlxG::mouse;
int32 FlxG::level = 0;
float32 FlxG::score = 0;
FlxSound FlxG::music;
const char* FlxG::LIBRARY_NAME = "flixel-x";


const char* FlxG::getLibraryName()
{
	return "flixel-x v1.0";
}

uint32 FlxG::getFramerate()
{
	return 1000 / _game->_step;
}

void FlxG::setFramerate(uint32 framerate)
{
	_game->_step = 1000 / framerate;
	if (_game->_maxAccumulation < _game->_step)
		_game->_maxAccumulation = _game->_step;
}

float32 FlxG::random()
{
	globalSeed = FlxU::srand(globalSeed);
	return ((float32)rand() / (float32)(RAND_MAX));;
}

void FlxG::resetState()
{
	_game->_requestedState = (FlxState*)GetClass(_game->_state)();
}

void FlxG::resetGame()
{
	_game->_requestedReset = true;
}

void FlxG::resetInput()
{
	keyboard.reset();
	mouse.reset();
}

void FlxG::playMusic(FlxAudio _music, float32 volume)
{
	music.loadAudio(_music, true);
	music.setVolume(volume);
	music.play();
}

void FlxG::playSound(FlxAudio sound, float32 volume, bool looped)
{
	FlxSound flxSound;
	flxSound.loadAudio(sound, looped);
	flxSound.setVolume(volume);
	flxSound.play();
}

float32 FlxG::getVolume()
{
	return _volume;
}

void FlxG::setVolume(float32 volume)
{
	_volume = volume;
}

void FlxG::destroySounds(bool forceDestroy)
{
	if (music.exists)
	{
		music.destroy();
	}
	FlxSound* sound = nullptr;
	size_t i = 0;
	size_t l = FlxG::sounds.length;
	while (i < l)
	{
		sound = (FlxSound*)FlxG::sounds.members[i++];
		if (sound != nullptr && sound->exists)
			sound->destroy();
	}
	sounds.members.clearAndDestroy();
	sounds.clear();
}

void FlxG::updateSounds()
{
	if (music.active)
	{
		music.update();
	}
	if (sounds.active)
	{
		sounds.update();
	}
}

void FlxG::pauseSounds()
{
	if (music.exists)
	{
		music.pause();
	}
	FlxSound* sound = nullptr;
	size_t i = 0;
	size_t l = FlxG::sounds.length;
	while (i < l)
	{
		sound = (FlxSound*)FlxG::sounds.members[i++];
		if (sound != nullptr && sound->exists)
			sound->pause();
	}
}

void FlxG::resumeSounds()
{
	if (music.exists)
	{
		music.play();
	}
	FlxSound* sound = nullptr;
	size_t i = 0;
	size_t l = FlxG::sounds.length;
	while (i < l)
	{
		sound = (FlxSound*)FlxG::sounds.members[i++];
		if (sound != nullptr && sound->exists)
			sound->resume();
	}
}

FlxImage FlxG::createBitmap(float32 width, float32 height, uint32 color, bool unique, const char* key)
{
	return FlxImage();
}

FlxImage FlxG::addBitmap(FlxImage graphic, bool reverse, bool unique, const char* key)
{
	return FlxImage();
}

FlxState* FlxG::getState()
{
	return _game->_state;
}

void FlxG::switchState(FlxState* state)
{
	_game->_requestedState = state;
}

void FlxG::addCamera(FlxCamera* camera)
{
	FlxG::cameras.push(camera);
}

void FlxG::removeCamera(FlxCamera* camera, bool destroy)
{
	if (destroy)
		camera->destroy();
}

void FlxG::resetCameras(FlxCamera* newCamera)
{
	FlxCamera* cam = nullptr;
	size_t i = 0;
	size_t l = FlxG::cameras.getLength();
	while (i < l)
	{
		cam = FlxG::cameras[i++];
		cam->destroy();
	}
	FlxG::cameras.clearAndDestroy();
	if (newCamera == nullptr)
	{
		newCamera = FlxCamera::Create(0, 0, FlxG::width, FlxG::height);
	}
	FlxG::addCamera(newCamera);
	FlxG::camera = newCamera;
}

void FlxG::flash(uint32 color, float32 duration, bool force)
{
	size_t i = 0;
	size_t l = FlxG::cameras.getLength();
	while (i < l)
	{
		FlxG::cameras[i++]->flash(color, duration, force);
	}
}

void FlxG::fade(uint32 color, float32 duration, bool force)
{
	size_t i = 0;
	size_t l = FlxG::cameras.getLength();
	while (i < l)
	{
		FlxG::cameras[i++]->fade(color, duration, force);
	}
}

void FlxG::shake(float32 intensity, float32 duration, bool force, uint32 direction)
{
	size_t i = 0;
	size_t l = FlxG::cameras.getLength();
	while (i < l)
	{
		FlxG::cameras[i++]->shake(intensity, duration, force, direction);
	}
}

uint32 FlxG::getBgColor()
{
	if (FlxG::camera == nullptr)
	{
		return 0xff000000;
	}
	else
	{
		return FlxG::camera->bgColor;
	}
}

void FlxG::setBgColor(uint32 color)
{
	size_t i = 0;
	size_t l = FlxG::cameras.getLength();
	while (i < l)
	{
		FlxG::cameras[i++]->bgColor = color;
	}
}

bool FlxG::overlap(FlxBasic* objectOrGroup1, FlxBasic* objectOrGroup2, FlxCollisionOverlap notifyCallback, FlxCollisionOverlap processCallback)
{
	if (objectOrGroup1 != nullptr)
	{
		objectOrGroup1 = FlxG::getState();
	}
	if (objectOrGroup2 == objectOrGroup1)
	{
		objectOrGroup2 = nullptr;
	}
	FlxQuadTree::divisions = FlxG::worldDivisions;
	FlxQuadTree quadTree(FlxG::worldBounds.x, FlxG::worldBounds.y, FlxG::worldBounds.width, FlxG::worldBounds.height);
	quadTree.load(objectOrGroup1, objectOrGroup1, notifyCallback, processCallback);
	bool result = quadTree.execute();
	quadTree.destroy();
	return result;
}

bool FlxG::collide(FlxBasic* objectOrGroup1, FlxBasic* objectOrGroup2, FlxCollisionOverlap notifyCallback)
{
	return overlap(objectOrGroup1, objectOrGroup2, notifyCallback, FlxObject::separate);
}

void FlxG::init(FlxGame* game, float32 width, float32 height, float32 zoom)
{
	FlxG::_game = game;
	FlxG::width = width;
	FlxG::height = height;
	FlxG::mute = false;
	FlxG::_volume = 0.5f;
	if (FlxG::canvas == nullptr)
	{
		FlxG::canvas = new FlxCanvas2D();
		FlxG::audioPlayer = new FlxAudioPlayer();
		FlxG::canvas->init(width, height);
		FlxG::audioPlayer->init();
	}
	FlxCamera::defaultZoom = zoom;
	FlxG::mouse.image = (FlxImage::loadEmbeddedImage((void*)FlxEmbeddedAssets::CURSOR_PIXELS, FlxEmbeddedAssets::CURSOR_WIDTH, FlxEmbeddedAssets::CURSOR_HEIGHT));
	FlxG::mobile = false;
	FlxG::visualDebug = false;
}

void FlxG::reset()
{
	FlxG::resetInput();
	FlxG::destroySounds(true);
	FlxG::level = 0;
	FlxG::score = 0;
	FlxG::paused = false;
	FlxG::timeScale = 1.0f;
	FlxG::elapsed = 0;
	FlxG::globalSeed = rand() % INT32_MAX;
	FlxG::worldBounds.make(-10, -10, FlxG::width + 20, FlxG::height + 20);
	FlxG::worldDivisions = 6;
}

void FlxG::updateInput()
{
	SDL_Event evt;
	while (SDL_PollEvent(&evt))
	{
		if (evt.type == SDL_MOUSEBUTTONDOWN)
		{
			mouse.setMouseButtonDown(evt.button.button);
			_game->onMouseDown(evt);
		}
		else if (evt.type == SDL_MOUSEBUTTONUP)
		{
			mouse.setMouseButtonUp(evt.button.button);
			_game->onMouseUp(evt);
		}
		else if (evt.type == SDL_MOUSEMOTION)
		{
			mouse.setMousePos((float32)evt.motion.x, (float32)evt.motion.y);
			_game->onMouseMove(evt);
		}
		else if (evt.type == SDL_KEYDOWN)
		{
			keyboard.setKeyDown(evt.key.keysym.sym);
			_game->onKeyDown(evt);
		}
		else if (evt.type == SDL_KEYUP)
		{
			keyboard.setKeyUp(evt.key.keysym.sym);
			_game->onKeyUp(evt);
		}
		else if (evt.type == SDL_WINDOWEVENT_FOCUS_GAINED)
		{
			_game->onFocus(evt);
		}
		else if (evt.type == SDL_WINDOWEVENT_FOCUS_LOST)
		{
			_game->onFocusLost(evt);
		}
		else if (evt.type == SDL_QUIT)
		{
			_game->exitGame();
		}
	}
}

void FlxG::lockCameras()
{
	FlxCamera* cam = nullptr;
	size_t i = 0;
	size_t l = cameras.getLength();
	while (i < l)
	{
		cam = cameras[i++];
		if (cam == nullptr || !cam->exists || !cam->visible)
			continue;
		cam->preDraw();
		cam->fill(cam->bgColor);
	}
}

void FlxG::unlockCameras()
{
	FlxCamera* cam = nullptr;
	size_t i = 0;
	size_t l = cameras.getLength();
	while (i < l)
	{
		cam = cameras[i++];
		if (cam == nullptr || !cam->exists || !cam->visible)
			continue;
		cam->postDraw();
	}	
}

void FlxG::updateCameras()
{
	FlxCamera* cam = nullptr;
	size_t i = 0;
	size_t l = cameras.getLength();
	while (i < l)
	{
		cam = cameras[i++];
		if (cam != nullptr && cam->exists && cam->active)
		{
			cam->update();
		}
	}
}

void FlxG::destroy()
{
	if (FlxG::canvas != nullptr)
	{
		delete FlxG::canvas;
		delete FlxG::audioPlayer;
		FlxImage::discardImage(FlxG::mouse.image);
	}
}
