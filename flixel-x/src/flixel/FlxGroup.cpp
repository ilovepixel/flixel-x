#include <flixel/FlxGroup.h>
#include <flixel/FlxU.h>

inline FlxGroup::FlxGroup(size_t maxSize) :
	FlxBasic(),
	length(0),
	members(1),
	_maxSize(maxSize),
	_marker(0),
	_sortIndex(0)
{

}

void FlxGroup::destroy()
{
	FlxBasic* basic = nullptr;
	size_t i = 0;
	while (i < length)
	{
		basic = members[i++];
		if (basic != nullptr)
		{
			basic->destroy();
		}
		members.clear();
	}
}

void FlxGroup::update()
{
	FlxBasic* basic = nullptr;
	size_t i = 0;
	while (i < length)
	{
		basic = members[i++];
		if (basic != nullptr)
		{
			basic->preUpdate();
			basic->update();
			basic->postUpdate();
		}
	}
}

void FlxGroup::draw()
{
	FlxBasic* basic = nullptr;
	size_t i = 0;
	while (i < length)
	{
		basic = members[i++];
		if (basic != nullptr)
		{
			basic->draw();
		}
	}
}

void FlxGroup::kill()
{
	FlxBasic* basic = nullptr;
	size_t i = 0;
	while (i < length)
	{
		basic = members[i++];
		if (basic != nullptr)
		{
			basic->kill();
		}
	}
}

size_t FlxGroup::getMaxSize()
{
	return _maxSize;
}

void FlxGroup::setMaxSize(size_t size)
{
	_maxSize = size;
	if (_marker >= _maxSize)
	{
		_marker = 0;
	}
	if (_maxSize == 0 || members.getLength() == 0 || _maxSize >= members.getLength())
	{
		return;
	}
	FlxBasic* basic = nullptr;
	size_t i = 0;
	size_t l = members.getLength();
	while (i < l)
	{
		basic = members[i++];
		if (basic != nullptr)
		{
			basic->destroy();
		}
	}
	length = _maxSize;
	members.reserve(_maxSize);
}

void FlxGroup::add(FlxBasic* object)
{
	if (members.contains(object))
	{
		return;
	}
	size_t i = 0;
	size_t l = members.getLength();
	while (i < l)
	{
		if (members[i] == nullptr)
		{
			members[i] = object;
			if (i >= length)
			{
				length = i + 1;
			}
			return;
		}
		++i;
	}
	if (_maxSize > 0)
	{
		if (members.getLength() >= _maxSize)
		{
			return;
		}
		else if (members.getLength() * 2 <= _maxSize)
		{
			members.reserve(members.getLength() * 2);
		}
		else
		{
			members.reserve(_maxSize);
		}
	}
	else
	{
		members.reserve(members.getLength() * 2);
	}
	members.push(object);
	length = i + 1;
}

void FlxGroup::remove(FlxBasic* object, bool splice)
{
	int32 index = members.indexOf(object);
	if (members.contains(object))
	{
		members[index] = nullptr;
	}
}

void FlxGroup::replace(FlxBasic* oldObject, FlxBasic* newObject)
{
	int32 index = members.indexOf(oldObject);
	if (index < 0 || index >= (int32)members.getLength())
	{
		return;
	}
	members[index] = newObject;
}

FlxBasic* FlxGroup::getFirstExtant()
{
	FlxBasic* basic = nullptr;
	size_t i = 0;
	while (i < length)
	{
		basic = members[i++];
		if (basic != nullptr && basic->exists)
		{
			return basic;
		}
	}
	return nullptr;
}

FlxBasic* FlxGroup::getFirstAlive()
{
	FlxBasic* basic = nullptr;
	size_t i = 0;
	while (i < length)
	{
		basic = members[i++];
		if (basic != nullptr && basic->alive)
		{
			return basic;
		}
	}
	return nullptr;
}

FlxBasic* FlxGroup::getFirstDead()
{
	FlxBasic* basic = nullptr;
	size_t i = 0;
	while (i < length)
	{
		basic = members[i++];
		if (basic != nullptr && !basic->alive)
		{
			return basic;
		}
	}
	return nullptr;
}

size_t FlxGroup::countLiving()
{
	FlxBasic* basic = nullptr;
	size_t i = 0;
	size_t count = 0;
	while (i < length)
	{
		basic = members[i++];
		if (basic != nullptr && basic->alive)
		{
			++count;
		}
	}
	return count;
}

size_t FlxGroup::countDead()
{
	FlxBasic* basic = nullptr;
	size_t i = 0;
	size_t count = 0;
	while (i < length)
	{
		basic = members[i++];
		if (basic != nullptr && !basic->alive)
		{
			++count;
		}
	}
	return count;
}

FlxBasic* FlxGroup::getRandom(size_t startIndex, size_t length)
{
	return FlxU::getRandom(&members[0], members.getLength(), startIndex, length);
}

void FlxGroup::clear()
{
	members.clear();
	length = 0;
}

FlxBasic::FlxBasicType FlxGroup::getType()
{
	return FlxBasic::FLXGROUP;
}
