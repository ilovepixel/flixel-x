#include <flixel/FlxSound.h>
#include <flixel/FlxU.h>
#include <flixel/FlxG.h>
#include <flixel/FlxObject.h>

FlxSound::FlxSound() :
	FlxBasic()
{}

FlxSound::~FlxSound()
{
	if (_sound.isLoaded())
		FlxAudio::discardAudio(_sound);
}

void FlxSound::destroy()
{
	kill();
	_target = nullptr;
	FlxBasic::destroy();
}

void FlxSound::update()
{
	if (_position != 0.0f)
		return;
	float32 radial = 1.0f;
	float32 fade = 1.0f;
	if (_target != nullptr)
	{
		radial = FlxU::getDistance(FlxPoint(_target->x, _target->y), FlxPoint(x, y)) / _radius;
		if (radial < 0) radial = 0;
		if (radial > 1) radial = 1;
		if (_pan)
		{
			float32 d = (_target->x - x) / _radius;
			if (d < -1) d = -1;
			else if (d > 1) d = 1;
		}
	}
	if (_fadeOutTimer > 0)
	{
		_fadeOutTimer -= FlxG::elapsed;
		if (_fadeOutTimer <= 0)
		{
			if (_pauseOnFadeOut)
			{
				pause();
			}
			else
			{
				stop();
			}
			fade = _fadeOutTimer / _fadeOutTotal;
			if (fade < 0) fade = 0;
		}
		else if (_fadeInTimer > 0)
		{
			_fadeInTimer -= FlxG::elapsed;
			fade = _fadeInTimer / _fadeInTotal;
			if (fade < 0) fade = 0;
			fade = 1 - fade;
		}
		_volumeAdjust = radial * fade;
		updateTransform();
	}
}

void FlxSound::kill()
{
	FlxBasic::kill();
	stop();
}

void FlxSound::createSound()
{
	destroy();
	x = 0;
	y = 0;
	_position = 0;
	_volume = 1.0;
	_volumeAdjust = 1.0;
	_looped = false;
	_target = nullptr;
	_radius = 0;
	_pan = false;
	_fadeOutTimer = 0;
	_fadeOutTotal = 0;
	_pauseOnFadeOut = false;
	_fadeInTimer = 0;
	_fadeInTotal = 0;
	exists = false;
	active = false;
	visible = false;
	amplitude = 0;
	amplitudeLeft = 0;
	amplitudeRight = 0;
	autoDestroy = false;
}

void FlxSound::loadAudio(FlxAudio audio, bool looped, bool autoDestroy)
{
	_sound = audio;
	_sound.setLoop(looped);
}

FlxSound& FlxSound::proximity(float32 _x, float32 _y, FlxObject* object, float32 radius, bool pan)
{
	x = _x;
	y = _y;
	_target = object;
	_radius = radius;
	_pan = pan;
	return *this;
}

void FlxSound::play(bool forceRestart)
{
	if (_sound.isLoaded())
		FlxG::audioPlayer->play(_sound);
}

void FlxSound::resume()
{}

void FlxSound::stop()
{
	if (_sound.isLoaded())
		FlxG::audioPlayer->stop(_sound);
}

void FlxSound::pause()
{}

void FlxSound::fadeOut(float32 seconds, bool pauseInstead)
{
	_pauseOnFadeOut = pauseInstead;
	_fadeInTimer = 0;
	_fadeOutTimer = seconds;
	_fadeOutTotal = _fadeOutTimer;
}

void FlxSound::fadeIn(float32 seconds)
{
	_fadeOutTimer = 0;
	_fadeInTimer = seconds;
	_fadeInTotal = _fadeInTimer;
	play();
}

float32 FlxSound::getVolume()
{
	return _volume;
}

void FlxSound::setVolume(float32 volume)
{
	if (_sound.isLoaded())
	{
		_sound.setGain(volume);
		_volume = volume;
	}
}

float32 FlxSound::getActualvolume()
{
	return _volume / _volumeAdjust;
}

void FlxSound::updateTransform()
{}
