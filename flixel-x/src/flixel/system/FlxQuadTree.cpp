#include <flixel/system/FlxQuadTree.h>
#include <flixel/FlxGroup.h>

uint32 FlxQuadTree::_min;
FlxObject* FlxQuadTree::_object;
float32 FlxQuadTree::_objectLeftEdge;
float32 FlxQuadTree::_objectRightEdge;
float32 FlxQuadTree::_objectTopEdge;
float32 FlxQuadTree::_objectBottomEdge;
uint32 FlxQuadTree::_list;
bool FlxQuadTree::_useBothList;
FlxList* FlxQuadTree::_iterator;
float32 FlxQuadTree::_objectHullX;
float32 FlxQuadTree::_objectHullY;
float32 FlxQuadTree::_objectHullWidth;
float32 FlxQuadTree::_objectHullHeight;
float32 FlxQuadTree::_checkObjectHullX;
float32 FlxQuadTree::_checkObjectHullY;
float32 FlxQuadTree::_checkObjectHullWidth;
float32 FlxQuadTree::_checkObjectHullHeight;
FlxQuadTree::FlxCallback FlxQuadTree::_processingCallback;
FlxQuadTree::FlxCallback FlxQuadTree::_notifyCallback;
uint32 FlxQuadTree::divisions = 0;

FlxQuadTree::FlxQuadTree(float32 x, float32 y, float32 width, float32 height, FlxQuadTree * parent) :
	FlxRect(x, y, width, height)
{
	_headA = _tailA = new FlxList();
	_headB = _tailB = new FlxList();
	if (parent != nullptr)
	{
		FlxList* iterator = nullptr;
		FlxList* ot = nullptr;
		if (parent->_headA->object != nullptr)
		{
			iterator = parent->_headA;
			while (iterator != nullptr)
			{
				if (_tailA->object != nullptr)
				{
					ot = _tailA;
					_tailA = new FlxList();
					ot->next = _tailA;
				}
				_tailA->object = iterator->object;
				iterator = iterator->next;
			}
		}
		if (parent->_headB->object != nullptr)
		{
			iterator = parent->_headB;
			while (iterator != nullptr)
			{
				if (_tailB->object != nullptr)
				{
					ot = _tailB;
					_tailB = new FlxList();
					ot->next = _tailB;
				}
				_tailB->object = iterator->object;
				iterator = iterator->next;
			}
		}
	}
	else
		_min = (uint32)((width + height) / (2 * divisions));
	_canSubdivide = (width > _min) || (height > _min);

	_northWestTree = nullptr;
	_northEastTree = nullptr;
	_southEastTree = nullptr;
	_southWestTree = nullptr;
	_leftEdge = x;
	_rightEdge = x + width;
	_halfWidth = width / 2;
	_midpointX = _leftEdge + _halfWidth;
	_topEdge = y;
	_bottomEdge = y + height;
	_halfHeight = height / 2;
	_midpointY = _topEdge + _halfHeight;
}

void FlxQuadTree::destroy()
{
	_headA->destroy();
	_tailA->destroy();
	_headB->destroy();
	_tailB->destroy();
	delete _headA;
	delete _headB;

	if (_northWestTree != nullptr)
	{
		_northWestTree->destroy();
		delete _northWestTree;
	}
	if (_northEastTree != nullptr)
	{
		_northEastTree->destroy();
		delete _northEastTree;
	}
	if (_southEastTree != nullptr)
	{
		_southEastTree->destroy();
		delete _southEastTree;
	}
	if (_southWestTree != nullptr)
	{
		_southWestTree->destroy();
		delete _southWestTree;
	}

	_object = nullptr;
	_processingCallback = nullptr;
	_notifyCallback = nullptr;
}

void FlxQuadTree::load(FlxBasic * objectOrGroup1, FlxBasic * objectOrGroup2, FlxCallback notifyCallback, FlxCallback processCallback)
{
	add(objectOrGroup1, A_LIST);
	if (objectOrGroup2 != nullptr)
	{
		add(objectOrGroup2, B_LIST);
		_useBothList = true;
	}
	else
		_useBothList = false;
	_notifyCallback = notifyCallback;
	_processingCallback = processCallback;
}

void FlxQuadTree::add(FlxBasic * objectOrGroup, uint32 list)
{
	_list = list;
	if (objectOrGroup->getType() == FlxBasic::FLXGROUP)
	{
		size_t i = 0;
		FlxBasic* basic = nullptr;
		FlxArray<FlxBasic*>& members = ((FlxGroup*)objectOrGroup)->members;
		size_t l = ((FlxGroup*)objectOrGroup)->length;
		while (i < l)
		{
			basic = members[i++];
			if ((basic != nullptr) && basic->exists)
			{
				if (basic->getType() == FlxBasic::FLXGROUP)
					add(basic, list);
				else if (basic->getType() == FlxBasic::FLXOBJECT)
				{
					_object = (FlxObject*)basic;
					if (_object->exists && _object->allowCollisions)
					{
						_objectLeftEdge = _object->x;
						_objectTopEdge = _object->y;
						_objectRightEdge = _object->x + _object->width;
						_objectBottomEdge = _object->y + _object->height;
						addObject();
					}
				}
			}
		}
	}
	else
	{
		_object = (FlxObject*)objectOrGroup;
		if (_object->exists && _object->allowCollisions)
		{
			_objectLeftEdge = _object->x;
			_objectTopEdge = _object->y;
			_objectRightEdge = _object->x + _object->width;
			_objectBottomEdge = _object->y + _object->height;
			addObject();
		}
	}
}

void FlxQuadTree::addObject()
{
	if (!_canSubdivide || ((_leftEdge >= _objectLeftEdge) && (_rightEdge <= _objectRightEdge) && (_topEdge >= _objectTopEdge) && (_bottomEdge <= _objectBottomEdge)))
	{
		addToList();
		return;
	}

	if ((_objectLeftEdge > _leftEdge) && (_objectRightEdge < _midpointX))
	{
		if ((_objectTopEdge > _topEdge) && (_objectBottomEdge < _midpointY))
		{
			if (_northWestTree == nullptr)
				_northWestTree = new FlxQuadTree(_leftEdge, _topEdge, _halfWidth, _halfHeight, this);
			_northWestTree->addObject();
			return;
		}
		if ((_objectTopEdge > _midpointY) && (_objectBottomEdge < _bottomEdge))
		{
			if (_southWestTree == nullptr)
				_southWestTree = new FlxQuadTree(_leftEdge, _midpointY, _halfWidth, _halfHeight, this);
			_southWestTree->addObject();
			return;
		}
	}
	if ((_objectLeftEdge > _midpointX) && (_objectRightEdge < _rightEdge))
	{
		if ((_objectTopEdge > _topEdge) && (_objectBottomEdge < _midpointY))
		{
			if (_northEastTree == nullptr)
				_northEastTree = new FlxQuadTree(_midpointX, _topEdge, _halfWidth, _halfHeight, this);
			_northEastTree->addObject();
			return;
		}
		if ((_objectTopEdge > _midpointY) && (_objectBottomEdge < _bottomEdge))
		{
			if (_southEastTree == nullptr)
				_southEastTree = new FlxQuadTree(_midpointX, _midpointY, _halfWidth, _halfHeight, this);
			_southEastTree->addObject();
			return;
		}
	}

	if ((_objectRightEdge > _leftEdge) && (_objectLeftEdge < _midpointX) && (_objectBottomEdge > _topEdge) && (_objectTopEdge < _midpointY))
	{
		if (_northWestTree == nullptr)
			_northWestTree = new FlxQuadTree(_leftEdge, _topEdge, _halfWidth, _halfHeight, this);
		_northWestTree->addObject();
	}
	if ((_objectRightEdge > _midpointX) && (_objectLeftEdge < _rightEdge) && (_objectBottomEdge > _topEdge) && (_objectTopEdge < _midpointY))
	{
		if (_northEastTree == nullptr)
			_northEastTree = new FlxQuadTree(_midpointX, _topEdge, _halfWidth, _halfHeight, this);
		_northEastTree->addObject();
	}
	if ((_objectRightEdge > _midpointX) && (_objectLeftEdge < _rightEdge) && (_objectBottomEdge > _midpointY) && (_objectTopEdge < _bottomEdge))
	{
		if (_southEastTree == nullptr)
			_southEastTree = new FlxQuadTree(_midpointX, _midpointY, _halfWidth, _halfHeight, this);
		_southEastTree->addObject();
	}
	if ((_objectRightEdge > _leftEdge) && (_objectLeftEdge < _midpointX) && (_objectBottomEdge > _midpointY) && (_objectTopEdge < _bottomEdge))
	{
		if (_southWestTree == nullptr)
			_southWestTree = new FlxQuadTree(_leftEdge, _midpointY, _halfWidth, _halfHeight, this);
		_southWestTree->addObject();
	}
}

void FlxQuadTree::addToList()
{
	FlxList* ot = nullptr;
	if (_list == A_LIST)
	{
		if (_tailA->object != nullptr)
		{
			ot = _tailA;
			_tailA = new FlxList();
			ot->next = _tailA;
		}
		_tailA->object = _object;
	}
	else
	{
		if (_tailB->object != nullptr)
		{
			ot = _tailB;
			_tailB = new FlxList();
			ot->next = _tailB;
		}
		_tailB->object = _object;
	}
	if (!_canSubdivide)
		return;
	if (_northWestTree != nullptr)
		_northWestTree->addToList();
	if (_northEastTree != nullptr)
		_northEastTree->addToList();
	if (_southEastTree != nullptr)
		_southEastTree->addToList();
	if (_southWestTree != nullptr)
		_southWestTree->addToList();
}

bool FlxQuadTree::execute()
{
	bool overlapProcessed = false;
	FlxList* iterator = nullptr;

	if (_headA->object != nullptr)
	{
		iterator = _headA;
		while (iterator != nullptr)
		{
			_object = iterator->object;
			if (_useBothList)
				_iterator = _headB;
			else
				_iterator = iterator->next;
			if (_object->exists && (_object->allowCollisions > 0) &&
				(_iterator != nullptr) && (_iterator->object != nullptr) &&
				_iterator->object->exists && overlapNode())
			{
				overlapProcessed = true;
			}
			iterator = iterator->next;
		}
	}

	if ((_northWestTree != nullptr) && _northWestTree->execute())
		overlapProcessed = true;
	if ((_northEastTree != nullptr) && _northEastTree->execute())
		overlapProcessed = true;
	if ((_southEastTree != nullptr) && _southEastTree->execute())
		overlapProcessed = true;
	if ((_southWestTree != nullptr) && _southWestTree->execute())
		overlapProcessed = true;

	return overlapProcessed;
}

bool FlxQuadTree::overlapNode()
{
	bool overlapProcessed = false;
	FlxObject* checkObject = nullptr;
	while (_iterator != nullptr)
	{
		if (!_object->exists || (_object->allowCollisions <= 0))
			break;

		checkObject = _iterator->object;
		if ((_object == checkObject) || !checkObject->exists || (checkObject->allowCollisions <= 0))
		{
			_iterator = _iterator->next;
			continue;
		}

		_objectHullX = (_object->x < _object->last.x) ? _object->x : _object->last.x;
		_objectHullY = (_object->y < _object->last.y) ? _object->y : _object->last.y;
		_objectHullWidth = _object->x - _object->last.x;
		_objectHullWidth = _object->width + ((_objectHullWidth > 0) ? _objectHullWidth : -_objectHullWidth);
		_objectHullHeight = _object->y - _object->last.y;
		_objectHullHeight = _object->height + ((_objectHullHeight > 0) ? _objectHullHeight : -_objectHullHeight);

		_checkObjectHullX = (checkObject->x < checkObject->last.x) ? checkObject->x : checkObject->last.x;
		_checkObjectHullY = (checkObject->y < checkObject->last.y) ? checkObject->y : checkObject->last.y;
		_checkObjectHullWidth = checkObject->x - checkObject->last.x;
		_checkObjectHullWidth = checkObject->width + ((_checkObjectHullWidth > 0) ? _checkObjectHullWidth : -_checkObjectHullWidth);
		_checkObjectHullHeight = checkObject->y - checkObject->last.y;
		_checkObjectHullHeight = checkObject->height + ((_checkObjectHullHeight > 0) ? _checkObjectHullHeight : -_checkObjectHullHeight);

		if ((_objectHullX + _objectHullWidth > _checkObjectHullX) &&
			(_objectHullX < _checkObjectHullX + _checkObjectHullWidth) &&
			(_objectHullY + _objectHullHeight > _checkObjectHullY) &&
			(_objectHullY < _checkObjectHullY + _checkObjectHullHeight))
		{
			if ((_processingCallback == nullptr) || _processingCallback(_object, checkObject))
				overlapProcessed = true;
			if (overlapProcessed && (_notifyCallback != nullptr))
				_notifyCallback(_object, checkObject);
		}
		_iterator = _iterator->next;
	}

	return overlapProcessed;
}
