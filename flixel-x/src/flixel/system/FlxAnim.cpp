#include <flixel/system/FlxAnim.h>

FlxAnim::FlxAnim(char* name, FlxArray<uint32> frames, float32 frameRate, bool looped) :
	name(name),
	frames(frames),
	delay(frameRate > 0 ? 1.0f / frameRate : 0),
	looped(looped)
{}

FlxAnim::FlxAnim(FlxAnim& anim) :
	name(anim.name),
	frames(anim.frames),
	delay(anim.delay),
	looped(anim.looped)
{}

void FlxAnim::destroy()
{
	frames.clearAndDestroy();
	name.clear();
}

FlxAnim::~FlxAnim()
{}

FLX_FORCEINLINE bool FlxAnim::operator==(FlxAnim& other)
{
	return name == other.name;
}
