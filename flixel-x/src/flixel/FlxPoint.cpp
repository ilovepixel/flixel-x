#include <flixel/FlxPoint.h>

FlxPoint::FlxPoint(float32 x, float32 y) :
	x(x), y(y)
{}

FlxPoint::FlxPoint(FlxPoint& point) :
	x(point.x), y(point.y)
{}

FlxPoint& FlxPoint::make(float32 x, float32 y)
{
	this->x = x;
	this->y = y;
	return *this;
}

FlxPoint& FlxPoint::copyFrom(FlxPoint& other)
{
	x = other.x;
	y = other.y;
	return *this;
}

FlxPoint& FlxPoint::copyTo(FlxPoint& other)
{
	other.x = x;
	other.y = y;
	return other;
}
