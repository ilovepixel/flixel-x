#include <flixel/FlxSprite.h>
#include <flixel/FlxG.h>
#include <flixel/FlxU.h>

FlxSprite::FlxSprite(float32 x, float32 y, FlxImage graphic) :
	FlxObject(x, y),
	scale(1.0f, 1.0f),
	_alpha(1.0f),
	_color(0x00ffffff),
	antialiasing(false),
	finished(false),
	_facing(RIGHT),
	_animations(1),
	_flipped(0),
	_curAnim(nullptr),
	_curFrame(0),
	_curIndex(0),
	_frameTimer(0)
{
	health = 1;
	cameras = nullptr;
	loadGraphic(graphic);
}

void FlxSprite::destroy()
{
	size_t i = 0;
	size_t l = _animations.getLength();
	while (i < l)
	{
		_animations[i]->destroy();
		delete _animations[i];
	}
	_animations.clear();
}

void FlxSprite::loadGraphic(FlxImage graphic, bool animated, bool reverse, float32 width, float32 height, bool unique)
{
	_pixels = graphic;
	if (reverse)
	{
		_flipped = (uint32)(_pixels.getWidth()) >> 1;
	}
	else
	{
		_flipped = 0;
	}
	if (width == 0)
	{
		if (animated)
		{
			width = _pixels.getHeight();
		}
		else if (_flipped > 0)
		{
			width = _pixels.getWidth() * 0.5f;
		}
		else
		{
			width = _pixels.getWidth();
		}
	}
	this->width = frameWidth = width;
	if (height == 0)
	{
		if (animated)
		{
			height = width;
		}
		else
		{
			height = _pixels.getHeight();
		}
	}
	this->height = frameHeight = height;
	resetHelpers();
}

void FlxSprite::makeGraphic(float32 width, float32 height, uint32 color, bool unique)
{
	loadGraphic(FlxImage(width, height, color));
}

FLX_FORCEINLINE void FlxSprite::resetHelpers()
{
	origin.make(frameWidth * 0.5f, frameHeight * 0.5f);
	frames = (uint32)((_pixels.getWidth() / frameWidth) * (_pixels.getHeight() / frameHeight));
	_curIndex = 0;
}

FLX_FORCEINLINE void FlxSprite::postUpdate()
{
	FlxObject::postUpdate();
	updateAnimation();
}

void FlxSprite::draw()
{
	if (_flickerTimer != 0)
	{
		_flicker = !_flicker;
		if (_flicker)
			return;
	}
	if (dirty)
		calcFrame();
	if (cameras == nullptr)
	{
		cameras = &FlxG::cameras;
	}
	FlxCamera* camera = nullptr;
	size_t i = 0;
	size_t l = cameras->getLength();
	FlxCanvas2D* canvas = FlxG::canvas;
	FlxU::getRGBA(_color, _tempColorArray);
	float32 r = _tempColorArray[0] / 255.0f;
	float32 g = _tempColorArray[1] / 255.0f;
	float32 b = _tempColorArray[2] / 255.0f;
	canvas->setColor(r, g, b, _alpha);
	while (i < l)
	{
		camera = cameras->get(i++);
		if (!onScreen(camera))
			continue;
		_point.x = x - (camera->scroll.x * scrollFactor.x) - offset.x;
		_point.y = y - (camera->scroll.y * scrollFactor.y) - offset.y;
		_point.x += (_point.x > 0) ? 0.0000001f : -0.0000001f;
		_point.y += (_point.y > 0) ? 0.0000001f : -0.0000001f;
		if (((angle == 0)) && (scale.x == 1) && (scale.y == 1))
		{
			canvas->drawImageRect(_pixels, _point.x, _point.y, _framePos.x, _framePos.y, frameWidth, frameHeight);
		}
		else
		{
			canvas->pushMatrix();
			canvas->translate(_point.x + origin.x, _point.y + origin.y);
			canvas->scale(scale.x, scale.y);
			if (angle != 0)
				canvas->rotate(angle * 0.017453293f);
			canvas->drawImageRect(_pixels, -origin.x, -origin.y, _framePos.x, _framePos.y, frameWidth, frameHeight);
			canvas->popMatrix();
		}
		++FlxBasic::_VISIBLECOUNT;
		if (FlxG::visualDebug && !ignoreDrawDebug)
			drawDebug(camera);
	}
}

void FlxSprite::fill(uint32 color)
{}

void FlxSprite::updateAnimation()
{
	if (_curAnim != nullptr && _curAnim->delay > 0 && (_curAnim->looped || !finished))
	{
		_frameTimer += FlxG::elapsed;
		while (_frameTimer > _curAnim->delay)
		{
			_frameTimer = _frameTimer - _curAnim->delay;
			if (_curFrame == _curAnim->frames.getLength() - 1)
			{
				if (_curAnim->looped)
					_curFrame = 0;
				finished = true;
			}
			else
			{
				++_curFrame;
			}
			_curIndex = _curAnim->frames[_curFrame];
			dirty = true;
		}
	}
	if (dirty)
		calcFrame();
}

FLX_FORCEINLINE void FlxSprite::drawFrame(bool force)
{
	if (force || dirty)
		calcFrame();
}

FLX_FORCEINLINE void FlxSprite::addAnimation(char * name, FlxArray<uint32> frames, float32 frameRate, bool looped)
{
	_animations.push(new FlxAnim(name, frames, frameRate, looped));
}

void FlxSprite::play(char * animName, bool force)
{
	// TODO: Use hash here!
	if (!force && (_curAnim != nullptr) && (_curAnim->name == animName) && (_curAnim->looped || !finished)) return;
	_curFrame = 0;
	_curIndex = 0;
	_frameTimer = 0;
	size_t i = 0;
	size_t l = _animations.getLength();
	while (i < l)
	{
		if (_animations[i]->name == animName)
		{
			_curAnim = _animations.get(i);
			if (_curAnim->delay <= 0)
				finished = true;
			else
				finished = false;
			_curIndex = _curAnim->frames[_curFrame];
			dirty = true;
			return;
		}
		i++;
	}
	printf("No animation by the name %s\n", animName);
}

FLX_FORCEINLINE void FlxSprite::randomFrame()
{
	_curAnim = nullptr;
	_curIndex = (uint32)(FlxG::random()*(_pixels.getWidth() / frameWidth));
	dirty = true;
}

FLX_FORCEINLINE void FlxSprite::setOriginToCorner()
{
	origin.make(0, 0);
}

FLX_FORCEINLINE void FlxSprite::centerOffsets(bool adjustPosition)
{
	offset.x = (frameWidth - width) * 0.5f;
	offset.y = (frameHeight - height) * 0.5f;
	if (adjustPosition)
	{
		x += offset.x;
		y += offset.y;
	}
}

FLX_FORCEINLINE FlxImage FlxSprite::getPixels()
{
	return _pixels;
}

FLX_FORCEINLINE void FlxSprite::setPixels(FlxImage pixels)
{
	_pixels = pixels;
	frameWidth = width = _pixels.getWidth();
	frameHeight = height = _pixels.getHeight();
}

FLX_FORCEINLINE uint32 FlxSprite::getFacing()
{
	return _facing;
}

FLX_FORCEINLINE void FlxSprite::setFacing(uint32 direction)
{
	if (_facing != direction)
		dirty = true;
	_facing = direction;
}

FLX_FORCEINLINE float32 FlxSprite::getAlpha()
{
	return _alpha;
}

FLX_FORCEINLINE void FlxSprite::setAlpha(float32 alpha)
{
	_alpha = alpha;
	dirty = true;
}

FLX_FORCEINLINE uint32 FlxSprite::getColor()
{
	return _color;
}

FLX_FORCEINLINE void FlxSprite::setColor(uint32 color)
{
	_color = color & 0x00ffffff;
	dirty = true;
}

FLX_FORCEINLINE uint32 FlxSprite::getFrame()
{
	return _curIndex;
}

FLX_FORCEINLINE void FlxSprite::setFrame(uint32 frame)
{
	_curAnim = nullptr;
	_curIndex = frame;
	dirty = true;
}

bool FlxSprite::onScreen(FlxCamera* camera)
{
	if (camera == nullptr)
		camera = FlxG::camera;
	_point = getScreenXY(camera);
	_point.x = _point.x - offset.x;
	_point.y = _point.y - offset.y;

	if (((angle == 0)) && (scale.x == 1) && (scale.y == 1))
		return ((_point.x + frameWidth > 0) && (_point.x < camera->width) && (_point.y + frameHeight > 0) && (_point.y < camera->height));

	float32 halfWidth = frameWidth / 2;
	float32 halfHeight = frameHeight / 2;
	float32 absScaleX = (scale.x > 0) ? scale.x : -scale.x;
	float32 absScaleY = (scale.y > 0) ? scale.y : -scale.y;
	float32 radius = sqrtf(halfWidth*halfWidth + halfHeight*halfHeight)*((absScaleX >= absScaleY) ? absScaleX : absScaleY);
	_point.x += halfWidth;
	_point.y += halfHeight;
	return ((_point.x + radius > 0) && (_point.x - radius < camera->width) && (_point.y + radius > 0) && (_point.y - radius < camera->height));
}

void FlxSprite::calcFrame()
{
	/*uint32 indexX = _curIndex * (uint32)frameWidth;
	uint32 indexY = 0;
	uint32 widthHelper = _flipped ? _flipped : (uint32)_pixels.getWidth();
	if (indexX >= widthHelper)
	{
		indexY = (uint32)(indexX / widthHelper) * (uint32)frameHeight;
		indexX %= widthHelper;
	}
	if (_flipped && (_facing == LEFT))
		indexX = (_flipped << 1) - indexX - (uint32)frameWidth;
	_framePos.x = (float32)indexX;
	_framePos.y = (float32)indexY;*/
	uint32 width = _pixels.getWidth();
	_framePos.y = (float32)(_curIndex / width) * frameWidth;
	_framePos.x = (float32)(_curIndex % width) * frameHeight;
	dirty = false;
}
