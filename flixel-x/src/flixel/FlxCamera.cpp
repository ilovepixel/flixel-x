#include <flixel/FlxCamera.h>
#include <flixel/FlxObject.h>
#include <flixel/FlxG.h>
#include <float.h>

float32 FlxCamera::defaultZoom = 1.0f;

FlxCamera::FlxCamera(float32 x, float32 y, float32 width, float32 height, float32 zoom) :
	FlxBasic(),
	x(x),
	y(y),
	width(width),
	height(height),
	target(nullptr),
	deadzone(),
	scroll(),
	_point(),
	bounds(),
	canvas(nullptr),
	_color(0xFFFFFFF),
	_fxFlashColor(0),
	_fxFlashDuration(0.0f),
	_fxFlashAlpha(0.0f),
	_fxFadeColor(0),
	_fxFadeDuration(0.0f),
	_fxFadeAlpha(0.0f),
	_fxShakeIntensity(0.0f),
	_fxShakeDuration(0.0f),
	_fxShakeOffset(),
	_fxShakeDirection(0),
	_angle(0)
{
	setZoom(zoom);
	bgColor = FlxG::getBgColor();
	canvas = FlxG::canvas;
	_colorArray[0] = 255;
	_colorArray[1] = 255;
	_colorArray[2] = 255;
	_colorArray[3] = 255;
	_tempColorArray[0] = 255;
	_tempColorArray[1] = 255;
	_tempColorArray[2] = 255;
	_tempColorArray[3] = 255;
}

void FlxCamera::destroy()
{}

void FlxCamera::update()
{
	if (target != nullptr)
	{
		if (deadzone.isZero())
		{
			focusOn(target->getMidpoint());
		}
		else
		{
			float32 edge = 0.0f;
			float32 targetX = target->x + (target->x > 0 ? 0.0000001f : -0.0000001f);
			float32 targetY = target->y + (target->y > 0 ? 0.0000001f : -0.0000001f);
			edge = targetX - deadzone.x;
			if (scroll.x > edge)
			{
				scroll.x = edge;
			}
			edge = targetX + target->width - deadzone.x - deadzone.width;
			if (scroll.x < edge)
			{
				scroll.x = edge;
			}
			edge = targetY - deadzone.y;
			if (scroll.y > edge)
			{
				scroll.y = edge;
			}
			edge = targetY + target->height - deadzone.y - deadzone.height;
			if (scroll.y < edge)
			{
				scroll.y = edge;
			}
		}
	}
	if (!bounds.isZero())
	{
		if (scroll.x < bounds.getLeft())
		{
			scroll.x = bounds.getLeft();
		}
		if (scroll.x > bounds.getRight() - width)
		{
			scroll.x = bounds.getRight() - width;
		}
		if (scroll.y < bounds.getTop())
		{
			scroll.y = bounds.getTop();
		}
		if (scroll.y > bounds.getBottom() - height)
		{
			scroll.y = bounds.getBottom() - height;
		}
	}
	if (_fxFlashAlpha > 0.0)
	{
		_fxFlashAlpha -= FlxG::elapsed / _fxFlashDuration;
	}

	if ((_fxFadeAlpha > 0.0) && (_fxFadeAlpha < 1.0))
	{
		_fxFadeAlpha += FlxG::elapsed / _fxFadeDuration;
		if (_fxFadeAlpha >= 1.0)
		{
			_fxFadeAlpha = 1.0;
		}
	}

	if (_fxShakeDuration > 0)
	{
		_fxShakeDuration -= FlxG::elapsed;
		if (_fxShakeDuration <= 0)
		{
			_fxShakeOffset.make(0, 0);
		}
		else
		{
			if ((_fxShakeDirection == SHAKE_BOTH_AXES) || (_fxShakeDirection == SHAKE_HORIZONTAL_ONLY))
				_fxShakeOffset.x = (FlxG::random()*_fxShakeIntensity*width * 2 - _fxShakeIntensity*width)*_zoom;
			if ((_fxShakeDirection == SHAKE_BOTH_AXES) || (_fxShakeDirection == SHAKE_VERTICAL_ONLY))
				_fxShakeOffset.y = (FlxG::random()*_fxShakeIntensity*height * 2 - _fxShakeIntensity*height)*_zoom;
		}
	}
}

void FlxCamera::follow(FlxObject* target, uint32 style)
{
	this->target = target;
	float32 helper = 0.0f;
	float32 w = width / 8;
	float32 h = height / 3;
	switch (style)
	{
		case STYLE_PLATFORMER:
			deadzone.make((width - w) / 2, (height - h) / 2 - h*0.25f, w, h);
			break;
		case STYLE_TOPDOWN:
			helper = FlxU::max(width, height) / 4;
			deadzone.make((width - helper) / 2, (height - helper) / 2, helper, helper);
			break;
		case STYLE_TOPDOWN_TIGHT:
			helper = FlxU::max(width, height) / 8;
			deadzone.make((width - helper) / 2, (height - helper) / 2, helper, helper);
			break;
		case STYLE_LOCKON:
		default:
			deadzone.zeroOut();
			break;
	}
}

void FlxCamera::focusOn(FlxPoint& point)
{
	point.x += (point.x > 0) ? 0.0000001f : -0.0000001f;
	point.y += (point.y > 0) ? 0.0000001f : -0.0000001f;
	scroll.make(point.x - width * 0.5f, point.y - height * 0.5f);
}

void FlxCamera::setBounds(float32 x, float32 y, float32 width, float32 height, bool updateWorld)
{
	bounds.make(x, y, width, height);
	if (updateWorld)
		FlxG::worldBounds.copyFrom(bounds);
	update();
}

void FlxCamera::flash(uint32 color, float32 duration, bool force)
{
	if (!force && (_fxFlashAlpha > 0.0f))
		return;
	_fxFlashColor = color;
	if (duration <= 0)
		duration = FLT_MIN;
	_fxFlashDuration = duration;
	_fxFlashAlpha = 1.0f;
}

void FlxCamera::fade(uint32 color, float32 duration, bool force)
{
	if (!force && (_fxFadeAlpha > 0.0))
		return;
	_fxFadeColor = color;
	if (duration <= 0)
		duration = FLT_MIN;
	_fxFadeDuration = duration;
	_fxFadeAlpha = FLT_MIN;
}

void FlxCamera::shake(float32 intensity, float32 duration, bool force, uint32 direction)
{
	if (!force && ((_fxShakeOffset.x != 0) || (_fxShakeOffset.y != 0)))
		return;
	_fxShakeIntensity = intensity;
	_fxShakeDuration = duration;
	_fxShakeDirection = direction;
	_fxShakeOffset.make(0, 0);
}

void FlxCamera::stopFX()
{
	_fxFlashAlpha = 0.0f;
	_fxFadeAlpha = 0.0f;
	_fxShakeDuration = 0;
}

FlxCamera& FlxCamera::copyFrom(FlxCamera& camera)
{
	if (camera.bounds.isZero())
		bounds.zeroOut();
	else
	{
		bounds.copyFrom(camera.bounds);
	}
	target = camera.target;
	if (target != nullptr)
	{
		if (camera.deadzone.isZero())
			deadzone.zeroOut();
		else
		{
			deadzone.copyFrom(camera.deadzone);
		}
	}
	return *this;
}

float32 FlxCamera::getZoom()
{
	return _zoom;
}

void FlxCamera::setZoom(float32 zoom)
{
	if (zoom == 0.0f)
	{
		_zoom = defaultZoom;
	}
	else
	{
		_zoom = zoom;
	}
	setScale(_zoom, _zoom);
}

float32 FlxCamera::getAlpha()
{
	return _colorArray[3] / 255.0f;
}

void FlxCamera::setAlpha(float32 alpha)
{
	_colorArray[3] = (uint8)(alpha * 255);
}

float32 FlxCamera::getAngle()
{
	return _angle;
}

void FlxCamera::setAngle(float32 angle)
{
	_angle = angle;
}

uint32 FlxCamera::getColor()
{
	return FlxU::makeColor(_colorArray[0], _colorArray[1], _colorArray[2], _colorArray[3]);
}

void FlxCamera::setColor(uint32 color)
{
	FlxU::getRGBA(color, _colorArray);
}

bool FlxCamera::isAntialiasing()
{
	return false;
}

void FlxCamera::setAntialiasing(bool antialiasing)
{}

FlxPoint FlxCamera::getScale()
{
	return _scale;
}

void FlxCamera::setScale(float32 x, float32 y)
{
	_scale.make(x, y);
}

void FlxCamera::fill(uint32 color)
{
	FlxU::getRGBA(color, _tempColorArray);
	float32 r = _tempColorArray[0] / 255.0f;
	float32 g = _tempColorArray[1] / 255.0f;
	float32 b = _tempColorArray[2] / 255.0f;
	float32 a = _tempColorArray[3] / 255.0f;
	canvas->setColor(r, g, b, a);
	canvas->drawRect(x, y, width, height);
}

void FlxCamera::fillRGBA(float32 r, float32 g, float32 b, float32 a)
{
	canvas->setColor(r / 255.0f, g / 255.0f, b / 255.0f, a);
	canvas->drawRect(x, y, width, height);
}

void FlxCamera::preDraw()
{
	canvas->scissor(x, y, width, height);
	canvas->pushMatrix();
	canvas->translate(
		x + _fxShakeOffset.x,
		y + _fxShakeOffset.y
	);
	canvas->scale(_scale.x, _scale.y);
	canvas->rotate(_angle);
}

void FlxCamera::postDraw()
{
	drawFX();
	canvas->popMatrix();
}

void FlxCamera::drawFX()
{
	float32 alphaComponent = 0.0f;

	if (_fxFlashAlpha > 0.0f)
	{
		FlxU::getRGBA(_fxFlashColor, _tempColorArray);
		fillRGBA(_tempColorArray[0], _tempColorArray[1], _tempColorArray[2], _fxFlashAlpha);
	}

	if (_fxFadeAlpha > 0.0)
	{
		FlxU::getRGBA(_fxFadeColor, _tempColorArray);
		fillRGBA(_tempColorArray[0], _tempColorArray[1], _tempColorArray[2], _fxFadeAlpha);
	}
}
