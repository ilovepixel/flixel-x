#include <flixel/FlxRect.h>

FlxRect::FlxRect(float32 x, float32 y, float32 width, float32 height) :
	x(x), y(y), width(width), height(height)
{}

FlxRect::FlxRect(FlxRect & rect) :
	x(rect.x), y(rect.y), width(rect.width), height(rect.height)
{}

bool FlxRect::isZero()
{
	return x == 0 && y == 0 && width == 0 && height == 0;
}

void FlxRect::zeroOut()
{
	make(0, 0, 0, 0);
}

float32 FlxRect::getLeft()
{
	return x;
}

float32 FlxRect::getRight()
{
	return x + width;
}

float32 FlxRect::getTop()
{
	return y;
}

float32 FlxRect::getBottom()
{
	return y + height;
}

FlxRect& FlxRect::make(float32 x, float32 y, float32 width, float32 height)
{
	this->x = x;
	this->y = y;
	this->width = width;
	this->height = height;
	return *this;
}

FlxRect& FlxRect::copyFrom(FlxRect& rect)
{
	x = rect.x;
	y = rect.y;
	width = rect.width;
	height = rect.height;
	return *this;
}

FlxRect& FlxRect::copyTo(FlxRect& rect)
{
	rect.x = x;
	rect.y = y;
	rect.width = width;
	rect.height = height;
	return rect;
}

bool FlxRect::overlaps(FlxRect& rect)
{
	return (rect.x + rect.width > x) &&
		(rect.x < x + width) &&
		(rect.y + rect.height > y) &&
		(rect.y < y + height);
}
