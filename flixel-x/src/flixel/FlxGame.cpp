#pragma once

#include <flixel/FlxGame.h>
#include <flixel/FlxU.h>
#include <flixel/FlxG.h>

FlxGame::FlxGame(float32 gameSizeX, float32 gameSizeY, FlxClass::Type initialState, float32 zoom, uint32 gameFrameRate, bool _useSystemCursor)
{
	_runGame = true;
	_lostFocus = false;
	FlxG::init(this, gameSizeX, gameSizeY, zoom);
	FlxG::setFramerate(gameFrameRate);
	_accumulator = _step;
	_total = 0;
	_state = nullptr;
	useSoundHotKeys = true;
	useSystemCursor = _useSystemCursor;
	if (!useSystemCursor)
	{
		SDL_ShowCursor(0);
	}
	forceDebugger = false;
	_debuggerUp = false;
	_replayRequested = false;
	_recrodingRequested = false;
	_replaying = false;
	_recording = false;
	_requestedState = nullptr;
	_requestedReset = true;
	_created = false;
	_iState = initialState;
	x = 0;
	y = 0;
	// On assets loaded loaded run create
	// For now we call immediately
	create();
}

FlxGame::~FlxGame()
{
	FlxG::destroy();
}

void FlxGame::showSoundTray(bool silent)
{}

void FlxGame::onKeyUp(SDL_Event sdlEvent)
{}

void FlxGame::onKeyDown(SDL_Event sdlEvent)
{}

void FlxGame::onMouseDown(SDL_Event sdlEvent)
{}

void FlxGame::onMouseUp(SDL_Event sdlEvent)
{}

void FlxGame::onMouseWheel(SDL_Event sdlEvent)
{}

void FlxGame::onMouseMove(SDL_Event sdlEvent)
{}

void FlxGame::onFocus(SDL_Event sdlEvent)
{
	if (!_debuggerUp && !useSystemCursor)
	{
		SDL_ShowCursor(0);
	}
	FlxG::resetInput();
	_lostFocus = false;
	FlxG::resumeSounds();
}

void FlxGame::onFocusLost(SDL_Event sdlEvent)
{
	if (x != 0 || y != 0)
	{
		x = 0;
		y = 0;
	}
	SDL_ShowCursor(1);
	_lostFocus = true;
	FlxG::pauseSounds();
}

void FlxGame::onEnterFrame()
{
	uint32 mark = FlxU::getTicks();
	uint32 elapsedMS = mark - _total / 1000;
	updateSoundTray(elapsedMS);
	if (!_lostFocus)
	{
		/*_accumulator += elapsedMS;
		if (_accumulator > _maxAccumulation)
		{
			_accumulator = _maxAccumulation;
		}
		while (_accumulator >= _step)
		{*/
			step();
		/*	_accumulator = _accumulator - _step;
		}*/
		FlxBasic::_VISIBLECOUNT = 0;
		draw();
	}
}

void FlxGame::switchState()
{
	FlxG::resetCameras();
	FlxG::resetInput();
	FlxG::destroySounds();
	if (_state != nullptr)
	{
		_state->destroy();
	}
	_state = _requestedState;
	_state->create();
}

void FlxGame::step()
{
	if (_requestedReset)
	{
		_requestedReset = false;
		_requestedState = (FlxState*)_iState();
		_replayTimer = 0;
		FlxG::reset();
	}
	if (_state != _requestedState)
	{
		switchState();
	}
	FlxBasic::_ACTIVECOUNT = 0;
	FlxG::updateInput();
	update();
}

void FlxGame::updateSoundTray(uint32 MS)
{}

void FlxGame::update()
{
	uint32 mark = FlxU::getTicks();
	FlxG::elapsed = (float32)(FlxG::timeScale * (_step / 1000.0f));
	FlxG::updateSounds();
	_state->update();
	FlxG::updateCameras();
}

void FlxGame::draw()
{
	uint32 mark = FlxU::getTicks();
	FlxG::canvas->clearScreen(0, 0, 0);
	FlxG::canvas->pushMatrix();
	FlxG::canvas->translate(x, y);
	FlxG::lockCameras();
	_state->draw();
	FlxG::unlockCameras();
	FlxG::canvas->popMatrix();
	if (!useSystemCursor)
	{
		FlxG::canvas->setColor(1, 1, 1);
		FlxG::canvas->setAlpha(1);
		FlxG::mouse.draw();
	}
	FlxG::canvas->flush();
	FlxG::canvas->flipBuffer();
}

void FlxGame::create()
{
	_total = FlxU::getTicks();
	if (!FlxG::mobile)
	{
		createSoundTray();
		createFocusScreen();
	}
	while (_runGame) 
		onEnterFrame();
}

void FlxGame::createSoundTray()
{}

void FlxGame::createFocusScreen()
{}

void FlxGame::exitGame()
{
	_runGame = false;
}
