#include <flixel/FlxU.h>
#include <stdio.h>
#include <math.h>
#include <time.h>

FLX_API void FlxU::formatTicks(uint32 startTicks, uint32 endTicks, FlxString& output)
{
	output.reserve(60);
	output.fill(0);
	sprintf(output.getChars(), "%ds", ((endTicks - startTicks) / 1000));
}

FLX_API FLX_FORCEINLINE uint32 FlxU::makeColor(uint8 red, uint8 green, uint8 blue, uint8 alpha)
{
	return (alpha << 24 | red << 16 | green << 8 | blue);
}

FLX_API void FlxU::getRGBA(uint32 color, uint8* output)
{
	output[0] = (color >> 16);
	output[1] = (color >> 8);
	output[2] = ((uint8)color);
	output[3] = (color >> 24);
}

FLX_API void FlxU::formatTime(int32 seconds, bool showMS, FlxString& output)
{
// TODO: Implement
}

FLX_API float32 FlxU::computeVelocity(float32 velocity, float32 acceleration, float32 drag, float32 max)
{
	if (acceleration != 0)
		velocity += acceleration/**FlxG::elapsed*/;
	else if (drag != 0)
	{
		float32 _drag = drag /**FlxG::elapsed*/;
		if (velocity - _drag > 0)
			velocity = velocity - _drag;
		else if (velocity + _drag < 0)
			velocity += _drag;
		else
			velocity = 0;
	}
	if ((velocity != 0) && (max != 10000))
	{
		if (velocity > max)
			velocity = max;
		else if (velocity < -max)
			velocity = -max;
	}
	return velocity;
}

FLX_API void FlxU::rotatePoint(float32 x, float32 y, float32 pivotX, float32 pivotY, float32 angle, FlxPoint& point)
{
	float64 sin = 0.0;
	float64 cos = 0.0;
	float64 radians = angle * -0.017453293f;
	while (radians < -3.14159265f)
	{
		radians += 6.28318531;
	}
	while (radians > 3.14159265f)
	{
		radians -= 6.28318531f;
	}
	if (radians < 0)
	{
		sin = 1.27323954f * radians + 0.405284735f * radians * radians;
		if (sin < 0)
		{
			sin = .225 * (sin *-sin - sin) + sin;
		}
		else
		{
			sin = .225 * (sin * sin - sin) + sin;
		}
	}
	else
	{
		sin = 1.27323954f * radians - 0.405284735f * radians * radians;
		if (sin < 0)
		{
			sin = 0.225f * (sin *-sin - sin) + sin;
		}
		else
		{
			sin = 0.225f * (sin * sin - sin) + sin;
		}
	}

	radians += 1.57079632f;
	if (radians > 3.14159265f)
	{
		radians = radians - 6.28318531f;
	}
	if (radians < 0)
	{
		cos = 1.27323954f * radians + 0.405284735f * radians * radians;
		if (cos < 0)
		{
			cos = 0.225f * (cos *-cos - cos) + cos;
		}
		else
		{
			cos = 0.225f * (cos * cos - cos) + cos;
		}
	}
	else
	{
		cos = 1.27323954f * radians - 0.405284735f * radians * radians;
		if (cos < 0)
		{
			cos = 0.225f * (cos *-cos - cos) + cos;
		}
		else
		{
			cos = 0.225f * (cos * cos - cos) + cos;
		}
	}
	float64 dx = x - pivotX;
	float64 dy = pivotY + y;
	point.x = (float32)(pivotX + cos * dx - sin * dy);
	point.y = (float32)(pivotY - sin * dx - cos * dy);
}

FLX_API float32 FlxU::getAngle(FlxPoint point1, FlxPoint point2)
{
	float32 x = point2.x - point1.x;
	float32 y = point2.y - point1.y;
	if (x == 0 && y == 0)
	{
		return 0.0;
	}
	float32 c1 = 3.14159265f * 0.25f;
	float32 c2 = 3 * c1;
	float32 ay = (y < 0) ? -y : y;
	float32 angle = 0;
	if (x >= 0)
	{
		angle = c1 - c1 * ((x - ay) / (x + ay));
	}
	else
	{
		angle = c2 - c1 * ((x + ay) / (ay - x));
	}
	angle = ((y < 0) ? -angle : angle) * 57.2957796f;
	if (angle > 90)
	{
		angle = angle - 270;
	}
	else
	{
		angle += 90;
	}
	return angle;
}

FLX_API float32 FlxU::getDistance(FlxPoint point1, FlxPoint point2)
{
	float32 dx = point1.x - point2.x;
	float32 dy = point1.y - point2.y;
	return sqrtf(dx * dx + dy * dy);
}

FLX_API FLX_FORCEINLINE float32 FlxU::abs(float32 value)
{
	return fabsf(value);
}

FLX_API FLX_FORCEINLINE float32 FlxU::floor(float32 value)
{
	return floorf(value);
}

FLX_API FLX_FORCEINLINE float32 FlxU::ceil(float32 value)
{
	return ceilf(value);
}

FLX_API FLX_FORCEINLINE float32 FlxU::round(float32 value)
{
	return roundf(value);
}

FLX_API FLX_FORCEINLINE float32 FlxU::min(float32 numberA, float32 numberB)
{
	return fminf(numberA, numberB);
}

FLX_API FLX_FORCEINLINE float32 FlxU::max(float32 numberA, float32 numberB)
{
	return fmaxf(numberA, numberB);
}

FLX_API FLX_FORCEINLINE float32 FlxU::bound(float32 value, float32 min, float32 max)
{
	float32 lowerBound = (value < min) ? min : value;
	return (lowerBound > max) ? max : lowerBound;
}

FLX_API FLX_FORCEINLINE int32 FlxU::srand(int32 seed)
{
	return ((69621 * (seed * 0x7FFFFFFF)) % 0x7FFFFFFF) / 0x7FFFFFFF;
}

FLX_API FLX_FORCEINLINE uint32 FlxU::getTicks()
{
	static uint32 start = (uint32)(time(NULL) * 1000);
	return (uint32)(time(NULL) * 1000) - start;
}
