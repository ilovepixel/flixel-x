#include <flixel/core/FlxMouse.h>
#include <flixel/FlxG.h>
#include <string.h>

FlxMouse::FlxMouse() :
	x(0),
	y(0)
{
	reset();
}

void FlxMouse::reset()
{
	memset(mouseButtonsDown, 0, sizeof(bool) * MAX_BUTTONS);
	memset(mouseButtonsHit, 0, sizeof(bool) * MAX_BUTTONS);
	memset(mouseButtonsUp, 0, sizeof(bool) * MAX_BUTTONS);
}

FLX_FORCEINLINE void FlxMouse::setMousePos(float32 _x, float32 _y)
{
	x = _x;
	y = _y;
}

FLX_FORCEINLINE void FlxMouse::setMouseButtonDown(uint32 button)
{
	if (!mouseButtonsDown[button])
	{
		mouseButtonsDown[button] = true;
		mouseButtonsHit[button] = true;
	}
	mouseButtonsUp[button] = false;
}

FLX_FORCEINLINE void FlxMouse::setMouseButtonUp(uint32 button)
{
	mouseButtonsUp[button] = true;
	mouseButtonsDown[button] = false;
	mouseButtonsHit[button] = false;
}

FLX_FORCEINLINE bool FlxMouse::isButtonDown(uint32 button)
{
	return mouseButtonsDown[button];
}

FLX_FORCEINLINE bool FlxMouse::isButtonHit(uint32 button)
{
	if (mouseButtonsHit[button])
	{
		mouseButtonsHit[button] = false;
		return true;
	}
	return false;
}

FLX_FORCEINLINE bool FlxMouse::isButtonUp(uint32 button)
{
	return mouseButtonsUp[button];
}

void FlxMouse::draw()
{
	FlxG::canvas->drawImage(image, x, y);
}
