#include <flixel/core/FlxShader.h>
#include <flixel/core/FlxError.h>

static void ogl_errorinfo(GLuint Object, PFNGLGETSHADERIVPROC Getiv, PFNGLGETSHADERINFOLOGPROC GetInfoLog)
{
	GLint info_length = 0;
	char *log = nullptr;
	Getiv(Object, GL_INFO_LOG_LENGTH, &info_length);
	log = new char[info_length + 1];
	GetInfoLog(Object, info_length, nullptr, log);
	log[info_length] = 0;
	fprintf(stderr, "Shader Error: %s", log);
	delete[] log;
}

GLuint CompileShader(const char* shaderSource, GLenum shaderType)
{
	GLint OK;
	GLuint shader = glCreateShader(shaderType);
	glShaderSource(shader, 1, &shaderSource, nullptr);
	glCompileShader(shader);
	glGetShaderiv(shader, GL_COMPILE_STATUS, &OK);
	if (!OK)
	{
		ogl_errorinfo(shader, glGetShaderiv, glGetShaderInfoLog);
		glDeleteShader(shader);
		if (shaderType == GL_VERTEX_SHADER)
		{
			FlxError("Couldn't compile vertex shader.");
		}
		else if (shaderType == GL_FRAGMENT_SHADER)
		{
			FlxError("Couldn't compile fragment shader.");
		}
		else
		{
			FlxError("Couldn't compiler shader.");
		}
	}
	return shader;
}

FlxShader::FlxShader() :
	vertexShader(0),
	fragmentShader(0),
	program(0)
{}

FlxShader::~FlxShader()
{
	if (program != 0)
	{
		discard();
	}
}

void FlxShader::build(const char * vertShaderSource, const char * fragShaderSource)
{
	GLint OK;
	program = glCreateProgram();
	vertexShader = CompileShader(vertShaderSource, GL_VERTEX_SHADER);
	fragmentShader = CompileShader(fragShaderSource, GL_FRAGMENT_SHADER);
	glAttachShader(program, vertexShader);
	glAttachShader(program, fragmentShader);
	glLinkProgram(program);
	glGetProgramiv(program, GL_LINK_STATUS, &OK);
	if (!OK)
	{
		ogl_errorinfo(program, glGetProgramiv, glGetProgramInfoLog);
		discard();
		FlxError("Couldn't link program.");
	}
}

void FlxShader::enable()
{
	glUseProgram(program);
}

void FlxShader::disable()
{
	glUseProgram(0);
}

void FlxShader::discard()
{
	glDeleteShader(vertexShader);
	glDeleteShader(fragmentShader);
	glDeleteProgram(program);
	program = 0;
	vertexShader = 0;
	fragmentShader = 0;
}

GLuint FlxShader::getAttribLocation(const char * name)
{
	return glGetAttribLocation(program, name);
}

GLuint FlxShader::getUniformLocation(const char * name)
{
	return glGetUniformLocation(program, name);
}
