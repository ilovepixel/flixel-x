#include <flixel/core/FlxImage.h>
#include <flixel/core/FlxError.h>
#include <flixel/core/FlxSIMD.h>
#include <string.h>

FlxImage::FlxImage() :
	width(0),
	height(0),
	texture(0),
	ID(0),
	state(NOT_LOADED)
{}

FlxImage::FlxImage(float32 width, float32 height) :
	width(width),
	height(height),
	texture(0),
	ID(0)
{
	glGenTextures(1, &texture);
	glBindTexture(GL_TEXTURE_2D, texture);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, (GLsizei)width, (GLsizei)height, 0, GL_RGBA, GL_UNSIGNED_BYTE, nullptr);
	glBindTexture(GL_TEXTURE_2D, 0);
	state = LOADED;
}

FlxImage::FlxImage(float32 width, float32 height, uint32 color) :
	width(width),
	height(height),
	texture(0),
	ID(0)
{
	uint8 r = (color >> 16);
	uint8 g = (color >> 8);
	uint8 b = ((uint8)color);
	uint8 a = (color >> 24);
	size_t size = (size_t)(width * height) * 4;
	size_t sizeInBytes = size * sizeof(uint8);
	uint8* pixels = (uint8*)_mm_malloc(sizeInBytes, 4);
	for (size_t index = 0; index < size; index += 4)
	{
		pixels[index] = r;
		pixels[index + 1] = g;
		pixels[index + 2] = b;
		pixels[index + 3] = a;
	}
	glGenTextures(1, &texture);
	glBindTexture(GL_TEXTURE_2D, texture);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, (GLsizei)width, (GLsizei)height, 0, GL_RGBA, GL_UNSIGNED_BYTE, pixels);
	glBindTexture(GL_TEXTURE_2D, 0);
	state = LOADED;
	_mm_free(pixels);
}

FLX_FORCEINLINE bool FlxImage::isLoaded()
{
	return state == LOADED;
}

FLX_FORCEINLINE const float32 FlxImage::getWidth()
{
	return width;
}

FLX_FORCEINLINE const float32 FlxImage::getHeight()
{
	return height;
}

const GLuint FlxImage::getTexture2D()
{
	return texture;
}

const GLenum FlxImage::getTextureID()
{
	return GL_TEXTURE0 + ID;
}

void FlxImage::readRGBAPixels(GLuint x, GLuint y, GLsizei width, GLsizei height, uint8* buffer)
{
	glReadPixels(x, y, width, height, GL_RGBA, GL_UNSIGNED_BYTE, buffer);
}

FlxImage FlxImage::loadImage(const char* path, FlxImageFormat format)
{
	FlxImage image;
	SDL_Surface* sdlSurface = nullptr;
	switch (format)
	{
		case FlxImage::PNG:
			sdlSurface = IMG_Load(path);
			break;
		case FlxImage::BMP:
			sdlSurface = SDL_LoadBMP(path);
			break;
		case FlxImage::TGA:
			sdlSurface = IMG_Load(path);
			break;
		case FlxImage::NONE:
		default:
			FlxError("( %s ) Invalid texture format.", path);
			break;
	}
	if (sdlSurface == nullptr)
	{
		image.state = FAILED;
	}
	else
	{
		image.width = (float32)sdlSurface->w;
		image.height = (float32)sdlSurface->h;
		image.state = IN_PROGRESS;
		glGenTextures(1, &image.texture);
		glBindTexture(GL_TEXTURE_2D, image.texture);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, (GLsizei)sdlSurface->w, (GLsizei)sdlSurface->h, 0, GL_RGBA, GL_UNSIGNED_BYTE, sdlSurface->pixels);
		glBindTexture(GL_TEXTURE_2D, 0);
		SDL_FreeSurface(sdlSurface);
		image.state = LOADED;
	}
	return image;
}

FlxImage FlxImage::loadEmbeddedImage(void* pixels, float32 width, float32 height)
{
	FlxImage image;
	image.width = width;
	image.height = height;
	image.state = IN_PROGRESS;
	glGenTextures(1, &image.texture);
	glBindTexture(GL_TEXTURE_2D, image.texture);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, (GLsizei)width, (GLsizei)height, 0, GL_RGBA, GL_UNSIGNED_BYTE, pixels);
	glBindTexture(GL_TEXTURE_2D, 0);
	image.state = LOADED;
	return image;
}

void FlxImage::discardImage(FlxImage image)
{
	if (image.state == LOADED)
	{
		glDeleteTextures(1, &image.texture);
		image.texture = 0;
		image.state = DISCARDED;
	}
}
