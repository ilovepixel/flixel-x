#include <flixel/core/FlxWAV.h>
#include <flixel/core/FlxLoad.h>
#include <string.h>
#include <assert.h>

static bool strCheck(char* str1, char* str2, size_t len)
{
	size_t index;
	for (index = 0; index < len; ++index)
	{
		if (str1[index] != str2[index])
			return false;
	}
	return true;
}

FlxWaveData* FlxWaveData::loadWAV(const char* path)
{
	size_t fileSize;
	void* pRawData = FlxLoad::loadFile(path, &fileSize);
	return parseWAV(pRawData);
}

void FlxWaveData::destroyWAV(FlxWaveData* waveData)
{
	FlxLoad::destroyFile(waveData);
}

FlxWaveData* FlxWaveData::parseWAV(void* rawData)
{
	if (rawData == nullptr)
		return nullptr;
	size_t size = sizeof(FlxWaveData);
	FlxWaveData* waveData = (FlxWaveData*)rawData;

	assert(strCheck((char*)waveData->chunkId, "RIFF", 4) && "Invalid WAV format");
	assert(strCheck((char*)waveData->format, "WAVE", 4) && "Invalid WAV format");
	assert(strCheck((char*)waveData->subchunk1Id, "fmt ", 4) && "Invalid WAV format");
	assert(strCheck((char*)waveData->subchunk2Id, "data", 4) && "Invalid WAV format");

	waveData->data = (void*)((char*)rawData + 44);
	return waveData;
}
