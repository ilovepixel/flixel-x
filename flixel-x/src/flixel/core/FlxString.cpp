#include <flixel/core/FlxString.h>
#include <string.h>

FlxString::FlxString(char* charArray) :
	FlxArray<char>(strlen(charArray) + 1)
{
	memset((void*)rawArray, 0, capacity);
	memmove(rawArray, charArray, capacity);
	length = capacity - 1;
}

FlxString::FlxString(FlxString& string) :
	FlxArray<char>(string.length - 1)
{
	memset((void*)rawArray, 0, capacity);
	memmove(rawArray, string.rawArray, capacity);
	length = capacity - 1;
}

FlxString& FlxString::operator=(char* charArray)
{
	size_t len = strlen(charArray);
	if (len > 0)
	{
		reserve(len + 1);
		memset((void*)rawArray, 0, capacity);
		memmove(rawArray, charArray, len);
	}
	else
	{
		memset((void*)rawArray, 0, capacity);
	}
	length = len;
	return *this;
}
/*
FlxString& FlxString::operator=(FlxString& string)
{
	size_t len = string.length;
	if (len > 0)
	{
		reserve(len + 1);
		memset((void*)rawArray, 0, capacity);
		memmove(rawArray, string.rawArray, len);
	}
	else
	{
		memset((void*)rawArray, 0, capacity);
	}
	length = len;
	return *this;
}*/

FlxString& FlxString::operator+=(char* charArray)
{
	size_t len = strlen(charArray);
	if (len > 0)
	{
		if (length + len > capacity)
		{
			reserve(length + len + 1);
		}
		memset((void*)(rawArray + length), 0, len + 1);
		memmove((rawArray + length), charArray, len);
		length += len;
	}
	return *this;
}

FlxString& FlxString::operator+=(FlxString& string)
{
	size_t len = string.length;
	if (len > 0)
	{
		if (length + len > capacity)
		{
			reserve(length + len + 1);
		}
		memset((void*)(rawArray + length), 0, len + 1);
		memmove((rawArray + length), string.rawArray, len);
		length += len;
	}
	return *this;
}

bool FlxString::operator==(char* charArray)
{
	return strcmp(rawArray, charArray) == 0;
}

bool FlxString::operator==(FlxString& string)
{
	return strcmp(rawArray, string.rawArray) == 0;
}

int32 FlxString::hash(size_t max)
{
	int32 hash = 0;
	int32 index = 0;
	int32 len = (int32)length;

	while (hash < INTMAX_MAX &&
		   index < len)
	{
		hash = hash << 8;
		hash = hash + rawArray[index];
		++index;
	}
	return hash % max;
}

void FlxString::fill(char&& value)
{
	memset(rawArray, value, capacity);
}

char * FlxString::getChars()
{
	return rawArray;
}
