#include <flixel/core/FlxLoad.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>
#include <immintrin.h>

void* FlxLoad::loadFile(const char* path, size_t* outFileSize)
{
	FILE* file = nullptr;
	char* charArray = nullptr;
	size_t fileSize = 0;
#if _WIN32
	fopen_s(&file, path, "rb");
#else
	pFile = fopen(filePath, "rb");
#endif
	assert(file != nullptr && "Couldn't open file");
	fseek(file, 0, SEEK_END);
	fileSize = (size_t)ftell(file);
	fseek(file, 0, SEEK_SET);
	charArray = (char*)_mm_malloc(fileSize + 1, 4);
	memset(charArray, 0, fileSize + 1);
	fread(charArray, fileSize, 1, file);
	fclose(file);
	*outFileSize = fileSize;
	return (void*)charArray;
}

void FlxLoad::destroyFile(void* fileData)
{
	_mm_free(fileData);
}
