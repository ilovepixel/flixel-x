#include <flixel/core/FlxKeyboard.h>
#include <string.h>

FlxKeyboard::FlxKeyboard()
{
	reset();
}

void FlxKeyboard::reset()
{
	memset(keysDown, 0, sizeof(bool) * MAX_KEYS);
	memset(keysHit, 0, sizeof(bool) * MAX_KEYS);
	memset(keysUp, 0, sizeof(bool) * MAX_KEYS);
}

FLX_FORCEINLINE void FlxKeyboard::setKeyDown(int32 key)
{
	if (key >= MAX_KEYS) return;
	if (!keysDown[key])
	{
		keysHit[key] = true;
		keysDown[key] = true;
	}
	keysUp[key] = false;
}

FLX_FORCEINLINE void FlxKeyboard::setKeyUp(int32 key)
{
	if (key >= MAX_KEYS) return;
	keysHit[key] = false;
	keysDown[key] = false;
	keysUp[key] = true;
}

FLX_FORCEINLINE bool FlxKeyboard::isKeyDown(int32 key)
{
	if (key >= MAX_KEYS) return false;
	return keysDown[key];
}

FLX_FORCEINLINE bool FlxKeyboard::isKeyHit(int32 key)
{
	if (key >= MAX_KEYS) return false;
	if (keysHit[key])
	{
		keysHit[key] = false;
		return true;
	}
	return false;
}

FLX_FORCEINLINE bool FlxKeyboard::isKeyUp(int32 key)
{
	if (key >= MAX_KEYS) return false;
	return keysUp[key];;
}
