#include <flixel/core/FlxWorker.h>

FlxWorker::FlxWorker() :
	runWorker(true),
	thread(std::thread(&FlxWorker::work, this))
{}

FlxWorker::~FlxWorker()
{
	wait();
	runWorker = false;
}

void FlxWorker::runJob(FlxJobType job)
{
	jobMutex.lock();
	jobs.push(job);
	jobMutex.unlock();
}

void FlxWorker::wait()
{
	while (jobs.getLength());
}

void FlxWorker::stop()
{
	runWorker = false;
}

std::thread::id FlxWorker::getThreadID()
{
	return thread.getThreadID();
}

void FlxWorker::work()
{
	while (runWorker)
	{
		if (jobs.getLength() > 0)
		{
			jobMutex.lock();
			FlxJobType& job = jobs.pop();
			if (job)
			{
				job();
			}
			jobMutex.unlock();
		}
	}
}
