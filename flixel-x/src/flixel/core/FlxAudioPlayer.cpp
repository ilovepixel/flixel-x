#pragma once

#include <flixel/core/FlxAudioPlayer.h>
#include <flixel/core/FlxError.h>

FLX_FORCEINLINE static void checkALError()
{
	switch (alGetError())
	{
		case AL_INVALID_NAME:
			FlxError("OpenAL: Invalide Source Name");
			break;
		case AL_INVALID_ENUM:
			FlxError("OpenAL: Invalid Property ID");
			break;
		case AL_INVALID_VALUE:
			FlxError("OpenAL: Invalid Volume or Frequency");
			break;
		case AL_INVALID_OPERATION:
			FlxError("OpenAL: Invalid Source Operation");
			break;
		case AL_OUT_OF_MEMORY:
			FlxError("OpenAL: Out of Memory");
			break;
	};
}

FlxAudioPlayer::FlxAudioPlayer() :
	device(nullptr),
	context(nullptr)
{}

FlxAudioPlayer::~FlxAudioPlayer()
{
	alcMakeContextCurrent(nullptr);
	if (context != nullptr)
		alcDestroyContext(context);
	if (device != nullptr)
		alcCloseDevice(device);
}

void FlxAudioPlayer::init()
{
	// Get Audio Device
	device = alcOpenDevice(nullptr);
	if (!device)
		FlxError("Failed to open audio device.");
	checkALError();
	// Create Context and Make it the current one.
	context = alcCreateContext(device, nullptr);
	if (!alcMakeContextCurrent(context))
		FlxError("Failed Context Setting");
	checkALError();
}

FLX_FORCEINLINE void FlxAudioPlayer::play(FlxAudio audio)
{
	alSourcePlay(audio.sourceId);
	checkALError();
}

FLX_FORCEINLINE void FlxAudioPlayer::stop(FlxAudio audio)
{
	alSourceStop(audio.sourceId);
	checkALError();
}