#include <flixel/core/FlxThread.h>
#include <utility>

FlxThread::FlxThread(FlxThread&& move) :
	stdThread(std::move(move.stdThread))
{}

FlxThread::FlxThread(std::thread && stdThreadMove) :
	stdThread(std::move(stdThreadMove))
{}

FlxThread::~FlxThread()
{
	stdThread.join();
}

FLX_FORCEINLINE std::thread::id FlxThread::getThreadID()
{
	return stdThread.get_id();
}
