#include <flixel/core/FlxCanvas2D.h>
#include <math.h>
#include <string.h>
#include <assert.h>

// Primitives Shader
const char* primVertSource =
"#version 120\n\n"
"precision mediump float;"
"attribute vec2 inVertexPos;"
"attribute vec4 inVertexCol;"
"uniform mat4 constViewMatrix;"
"varying vec4 outVertexCol;"
"void main() {"
"	gl_Position = constViewMatrix * vec4(inVertexPos, 1.0, 1.0);"
"	outVertexCol = inVertexCol;"
"}"
;
const char* primFragSource =
"#version 120\n\n"
"precision mediump float;"
"varying vec4 outVertexCol;"
"void main() {"
"	gl_FragColor = outVertexCol;"
"}"
;

// Texture Shader
const char* texVertSource =
"#version 120\n\n"
"precision mediump float;"
"attribute vec2 inVertexPos;"
"attribute vec4 inVertexCol;"
"attribute vec2 inTexCoords;"
"uniform mat4 constViewMatrix;"
"varying vec4 outVertexCol;"
"varying vec2 outTexCoords;"
"void main() {"
"	gl_Position = constViewMatrix * vec4(inVertexPos, 1.0, 1.0);"
"	outVertexCol = inVertexCol;"
"	outTexCoords = inTexCoords;"
"}"
;
const char* texFragSource =
"#version 120\n\n"
"precision mediump float;"
"varying vec4 outVertexCol;"
"varying vec2 outTexCoords;"
"uniform sampler2D mainSampler;"
"void main() {"
"	gl_FragColor = texture2D(mainSampler, outTexCoords) * outVertexCol;"
"}"
;

FlxCanvas2D::FlxCanvas2D() :
	sdlWindow(nullptr),
	spriteCount(0),
	currentDrawMode(NONE),
	currentGLDrawMode(GL_TRIANGLES),
	currentVertexCount(0)
{
	color[0] = 1.0f;
	color[1] = 1.0f;
	color[2] = 1.0f;
	color[3] = 1.0f;
}

FlxCanvas2D::~FlxCanvas2D()
{
	if (sdlWindow != nullptr)
	{
		glDeleteBuffers(1, &vertexPositionVBO);
		glDeleteBuffers(1, &vertexColorVBO);
		glDeleteBuffers(1, &texCoordsVBO);

		SDL_GL_DeleteContext(sdlGLContext);
		SDL_DestroyWindow(sdlWindow);
	}
}

void FlxCanvas2D::init(float32 width, float32 height, const char* title, bool fullscreen)
{
	// Init Context
	this->width = width;
	this->height = height;
	assert(!(SDL_Init(SDL_INIT_EVERYTHING) < 0) && "SDL_Init failed:" && SDL_GetError());
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 2);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 1);
	SDL_GL_SetAttribute(SDL_GL_ACCELERATED_VISUAL, 1);
	SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
	sdlWindow = SDL_CreateWindow(
		title,
		SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
		(int32)width, (int32)height,
		SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN | (fullscreen ? SDL_WINDOW_FULLSCREEN : 0)
	);
	assert(sdlWindow != nullptr && "Couldn't create window.");
	sdlGLContext = SDL_GL_CreateContext(sdlWindow);
	assert(sdlGLContext != nullptr && "Couldn't create OpenGL Context.");
	assert(glewInit() == GLEW_OK && "Couldn't initialize GLEW.");
	SDL_GL_MakeCurrent(sdlWindow, sdlGLContext);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
	SDL_GL_SetSwapInterval(1);

	// Init Shaders
	primitiveShader.build(primVertSource, primFragSource);
	textureShader.build(texVertSource, texFragSource);

	// Init Buffers
	glGenBuffers(1, &vertexPositionVBO);
	glGenBuffers(1, &vertexColorVBO);
	glGenBuffers(1, &texCoordsVBO);

	glBindBuffer(GL_ARRAY_BUFFER, vertexPositionVBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(float32) * FLX_MAX_VERTEICES * 2, nullptr, GL_DYNAMIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, vertexColorVBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(float32) * FLX_MAX_VERTEICES * 4, nullptr, GL_STATIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, texCoordsVBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(float32) * FLX_MAX_VERTEICES * 2, nullptr, GL_STATIC_DRAW);

	GLuint inVertexPosLoc;
	GLuint inVertexColLoc;
	GLuint inTexCoordsLoc;

	float32 viewMatrix[16] = {
		2.0f / width, 0, 0, 0,
		0, -2.0f / height, 0, 0,
		0, 0, 1.0f, 1.0f,
		-1.0f, 1.0f, 0, 0
	};

	primitiveShader.enable();
	glUniformMatrix4fv(primitiveShader.getUniformLocation("constViewMatrix"), 1, GL_FALSE, viewMatrix);
	inVertexPosLoc = primitiveShader.getAttribLocation("inVertexPos");
	inVertexColLoc = primitiveShader.getAttribLocation("inVertexCol");
	glBindBuffer(GL_ARRAY_BUFFER, vertexPositionVBO);
	glEnableVertexAttribArray(inVertexPosLoc);
	glVertexAttribPointer(inVertexPosLoc, 2, GL_FLOAT, GL_FALSE, 0, nullptr);

	glBindBuffer(GL_ARRAY_BUFFER, vertexColorVBO);
	glEnableVertexAttribArray(inVertexColLoc);
	glVertexAttribPointer(inVertexColLoc, 4, GL_FLOAT, GL_FALSE, 0, nullptr);

	textureShader.enable();
	glUniformMatrix4fv(textureShader.getUniformLocation("constViewMatrix"), 1, GL_FALSE, viewMatrix);
	inVertexPosLoc = textureShader.getAttribLocation("inVertexPos");
	inVertexColLoc = textureShader.getAttribLocation("inVertexCol");
	inTexCoordsLoc = textureShader.getAttribLocation("inTexCoords");
	glBindBuffer(GL_ARRAY_BUFFER, vertexPositionVBO);
	glEnableVertexAttribArray(inVertexPosLoc);
	glVertexAttribPointer(inVertexPosLoc, 2, GL_FLOAT, GL_FALSE, 0, nullptr);

	glBindBuffer(GL_ARRAY_BUFFER, vertexColorVBO);
	glEnableVertexAttribArray(inVertexColLoc);
	glVertexAttribPointer(inVertexColLoc, 4, GL_FLOAT, GL_FALSE, 0, nullptr);

	glBindBuffer(GL_ARRAY_BUFFER, texCoordsVBO);
	glEnableVertexAttribArray(inTexCoordsLoc);
	glVertexAttribPointer(inTexCoordsLoc, 2, GL_FLOAT, GL_FALSE, 0, nullptr);

	glUniform1i(textureShader.getUniformLocation("mainSampler"), 0);
}

FLX_FORCEINLINE void FlxCanvas2D::pushMatrix()
{
	matrixStack.push(currentMatrix);
}

FLX_FORCEINLINE void FlxCanvas2D::popMatrix()
{
	currentMatrix = matrixStack.pop();
}

void FlxCanvas2D::translate(float32 x, float32 y)
{
	FlxMat44 mat(
		1, 0, 0, x,
		0, 1, 0, y,
		0, 0, 1, 0,
		0, 0, 0, 1
	);
	currentMatrix = FlxMat44::mul(currentMatrix, mat);
}

void FlxCanvas2D::scale(float32 x, float32 y)
{
	FlxMat44 mat(
		x, 0, 0, 0,
		0, y, 0, 0,
		0, 0, 1, 0,
		0, 0, 0, 1
	);
	currentMatrix = FlxMat44::mul(currentMatrix, mat);
}

void FlxCanvas2D::rotate(float32 t)
{
	float32 tc = cosf(t);
	float32 ts = sinf(t);
	FlxMat44 mat(
		tc, -ts, 0, 0,
		ts, tc, 0, 0,
		0, 0, 1, 0,
		0, 0, 0, 1
	);
	currentMatrix = FlxMat44::mul(currentMatrix, mat);
}

FLX_FORCEINLINE void FlxCanvas2D::setColor(float32 r, float32 g, float32 b, float32 a)
{
	color[0] = r;
	color[1] = g;
	color[2] = b;
	color[3] = a;
}

FLX_FORCEINLINE void FlxCanvas2D::setColor(float32 r, float32 g, float32 b)
{
	color[0] = r;
	color[1] = g;
	color[2] = b;
}

FLX_FORCEINLINE void FlxCanvas2D::setAlpha(float32 a)
{
	color[3] = a;
}

void FlxCanvas2D::scissor(float32 x, float32 y, float32 w, float32 h)
{
	if (x != 0 || y != 0 || w != width || h != height)
	{
		glEnable(GL_SCISSOR_TEST);
		glScissor((GLint)x, (GLint)(height - y - h), (GLsizei)w, (GLsizei)h);
	}
	else
	{
		glDisable(GL_SCISSOR_TEST);
	}
}

void FlxCanvas2D::drawLine(float32 x1, float32 y1, float32 x2, float32 y2)
{
	pushBatch(6, GL_LINES, PRIMITIVE);
	float32 r = color[0];
	float32 g = color[1];
	float32 b = color[2];
	float32 a = color[3];
	float32 colors[8] =
	{
		r, g, b, a,
		r, g, b, a
	};
	float32 uv[4] =
	{
		0, 0,
		1, 1
	};
	float32* data = currentMatrix.scalarCols;
	// Apply Transform
	FlxVec4 v0(x1, y1, 1.0f, 1.0f);
	FlxVec4 v1(x2, y2, 1.0f, 1.0f);

	v0 = FlxMat44::mulMat44Vec4(currentMatrix, v0);
	v1 = FlxMat44::mulMat44Vec4(currentMatrix, v1);

	float32 vertexPos[4] =
	{
		v0.x, v0.y,
		v1.x, v1.y
	};

	// Store Values
	vertexPositionData.pushBatch(vertexPos, 4);
	vertexColorData.pushBatch(colors, 8);
	texCoordsData.pushBatch(uv, 4);
}

void FlxCanvas2D::drawCircle(float32 x, float32 y, float32 radius)
{
	static const float32 toRad = 3.141592f / 180;
	// Force flush
	flush();
	pushBatch(36, GL_TRIANGLE_FAN, PRIMITIVE);
	FlxVec4 vertices[36];
	float32 vertexPos[36 * 2];
	float32 uv[36 * 2];
	float32 colors[36 * 4];
	float32 r = color[0];
	float32 g = color[1];
	float32 b = color[2];
	float32 a = color[3];
	for (uint8 index = 0; index < 36; ++index) 
	{
		vertices[index] = FlxVec4(x + cosf(index * 10 * toRad) * radius, y + sinf(index * 10 * toRad) * radius, 1.0f, 1.0f);
	}
	for (uint8 index = 0; index < 36; ++index)
	{
		vertices[index] = FlxMat44::mulMat44Vec4(currentMatrix, vertices[index]);
	}
	uint8 count = 0;
	uint16 colorCount = 0;
	for (uint8 index = 0; index < 36; ++index)
	{
		vertexPos[count++] = vertices[index].x;
		vertexPos[count++] = vertices[index].y;
		colors[colorCount++] = r;
		colors[colorCount++] = g;
		colors[colorCount++] = b;
		colors[colorCount++] = a;
	}
	// Store Values
	vertexPositionData.pushBatch(vertexPos, 36 * 2);
	vertexColorData.pushBatch(colors, 36 * 4);
	texCoordsData.pushBatch(uv, 36 * 2);
}

void FlxCanvas2D::drawRect(float32 x, float32 y, float32 width, float32 height)
{
	pushBatch(6, GL_TRIANGLES, PRIMITIVE);
	float32 xw = x + width;
	float32 yh = y + height;
	float32 r = color[0];
	float32 g = color[1];
	float32 b = color[2];
	float32 a = color[3];
	float32 colors[24] =
	{
		r, g, b, a,
		r, g, b, a,
		r, g, b, a,
		r, g, b, a,
		r, g, b, a,
		r, g, b, a
	};
	float32 uv[12] =
	{
		0, 0,
		1, 1,
		0, 1,
		0, 0,
		1, 0,
		1, 1
	};
	float32* data = currentMatrix.scalarCols;	
	// Apply Transform
	FlxVec4 v0(x, y, 1.0f, 1.0f);
	FlxVec4 v1(xw, yh, 1.0f, 1.0f);
	FlxVec4 v2(x, yh, 1.0f, 1.0f);
	FlxVec4 v3(xw, y, 1.0f, 1.0f);

	v0 = FlxMat44::mulMat44Vec4(currentMatrix, v0);
	v1 = FlxMat44::mulMat44Vec4(currentMatrix, v1);
	v2 = FlxMat44::mulMat44Vec4(currentMatrix, v2);
	v3 = FlxMat44::mulMat44Vec4(currentMatrix, v3);

	float32 vertexPos[12] =
	{
		v0.x, v0.y,
		v1.x, v1.y,
		v2.x, v2.y,
		v0.x, v0.y,
		v3.x, v3.y,
		v1.x, v1.y
	};

	// Store Values
	vertexPositionData.pushBatch(vertexPos, 12);
	vertexColorData.pushBatch(colors, 24);
	texCoordsData.pushBatch(uv, 12);
}

void FlxCanvas2D::drawRectBorder(float32 x, float32 y, float32 width, float32 height)
{
	pushBatch(8, GL_LINES, PRIMITIVE);

	float32 x1 = x;
	float32 y1 = y;
	float32 x2 = x + width;
	float32 y2 = y + height;

	float32 r = color[0];
	float32 g = color[1];
	float32 b = color[2];
	float32 a = color[3];
	float32 colors[32] =
	{
		r, g, b, a,
		r, g, b, a,
		r, g, b, a,
		r, g, b, a,
		r, g, b, a,
		r, g, b, a,
		r, g, b, a,
		r, g, b, a
	};
	float32 uv[16] =
	{
		0, 0,
		1, 1,
		0, 1,
		0, 0,
		1, 0,
		1, 1,
		1, 1,
		1, 1
	};
	float32* data = currentMatrix.scalarCols;
	// Apply Transform
	FlxVec4 v0(x1, y1, 1.0f, 1.0f);
	FlxVec4 v1(x2, y1, 1.0f, 1.0f);
	FlxVec4 v2(x2, y2, 1.0f, 1.0f);
	FlxVec4 v3(x1, y2, 1.0f, 1.0f);

	v0 = FlxMat44::mulMat44Vec4(currentMatrix, v0);
	v1 = FlxMat44::mulMat44Vec4(currentMatrix, v1);
	v2 = FlxMat44::mulMat44Vec4(currentMatrix, v2);
	v3 = FlxMat44::mulMat44Vec4(currentMatrix, v3);

	float32 vertexPos[16] =
	{
		v0.x, v0.y,
		v1.x, v1.y,
		v1.x, v1.y,
		v2.x, v2.y,
		v2.x, v2.y,
		v3.x, v3.y,
		v3.x, v3.y,
		v0.x, v0.y
	};

	// Store Values
	vertexPositionData.pushBatch(vertexPos, 16);
	vertexColorData.pushBatch(colors, 32);
	texCoordsData.pushBatch(uv, 16);
}

void FlxCanvas2D::drawImage(FlxImage image, float32 x, float32 y)
{
	pushBatch(6, GL_TRIANGLES, TEXTURE, image);
	float32 xw = x + image.getWidth();
	float32 yh = y + image.getHeight();
	float32 r = color[0];
	float32 g = color[1];
	float32 b = color[2];
	float32 a = color[3];
	float32 colors[24] =
	{
		r, g, b, a,
		r, g, b, a,
		r, g, b, a,
		r, g, b, a,
		r, g, b, a,
		r, g, b, a
	};
	float32 uv[12] =
	{
		0, 0,
		1, 1,
		0, 1,
		0, 0,
		1, 0,
		1, 1
	};
	float32* data = currentMatrix.scalarCols;
	// Apply Transform
	FlxVec4 v0(x, y, 1.0f, 1.0f);
	FlxVec4 v1(xw, yh, 1.0f, 1.0f);
	FlxVec4 v2(x, yh, 1.0f, 1.0f);
	FlxVec4 v3(xw, y, 1.0f, 1.0f);

	v0 = FlxMat44::mulMat44Vec4(currentMatrix, v0);
	v1 = FlxMat44::mulMat44Vec4(currentMatrix, v1);
	v2 = FlxMat44::mulMat44Vec4(currentMatrix, v2);
	v3 = FlxMat44::mulMat44Vec4(currentMatrix, v3);

	float32 vertexPos[12] =
	{
		v0.x, v0.y,
		v1.x, v1.y,
		v2.x, v2.y,
		v0.x, v0.y,
		v3.x, v3.y,
		v1.x, v1.y
	};

	// Store Values
	vertexPositionData.pushBatch(vertexPos, 12);
	vertexColorData.pushBatch(colors, 24);
	texCoordsData.pushBatch(uv, 12);
}

void FlxCanvas2D::drawImageRect(FlxImage image, float32 x, float32 y, float32 rx, float32 ry, float32 rw, float32 rh)
{
	pushBatch(6, GL_TRIANGLES, TEXTURE, image);
	float32 crx = rx / image.getWidth();
	float32 cry = ry / image.getHeight();
	float32 crw = rw / image.getWidth();
	float32 crh = rh / image.getHeight();
	float32 xw = x + rw;
	float32 yh = y + rh;

	float32 r = color[0];
	float32 g = color[1];
	float32 b = color[2];
	float32 a = color[3];
	float32 colors[24] =
	{
		r, g, b, a,
		r, g, b, a,
		r, g, b, a,
		r, g, b, a,
		r, g, b, a,
		r, g, b, a
	};
	float32 uv[12] =
	{
		crx, cry,
		crx + crw, cry + crh,
		crx, cry + crh,
		crx, cry,
		crx + crw, cry,
		crx + crw, cry + crh
	};
	float32* data = currentMatrix.scalarCols;
	// Apply Transform
	FlxVec4 v0(x, y, 1.0f, 1.0f);
	FlxVec4 v1(xw, yh, 1.0f, 1.0f);
	FlxVec4 v2(x, yh, 1.0f, 1.0f);
	FlxVec4 v3(xw, y, 1.0f, 1.0f);

	v0 = FlxMat44::mulMat44Vec4(currentMatrix, v0);
	v1 = FlxMat44::mulMat44Vec4(currentMatrix, v1);
	v2 = FlxMat44::mulMat44Vec4(currentMatrix, v2);
	v3 = FlxMat44::mulMat44Vec4(currentMatrix, v3);

	float32 vertexPos[12] =
	{
		v0.x, v0.y,
		v1.x, v1.y,
		v2.x, v2.y,
		v0.x, v0.y,
		v3.x, v3.y,
		v1.x, v1.y
	};

	// Store Values
	vertexPositionData.pushBatch(vertexPos, 12);
	vertexColorData.pushBatch(colors, 24);
	texCoordsData.pushBatch(uv, 12);
}

FLX_FORCEINLINE void FlxCanvas2D::clearScreen(float32 r, float32 g, float32 b)
{
	scissor(0, 0, width, height);
	glClearColor(r, g, b, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT);
}

void FlxCanvas2D::flush()
{
	if (currentVertexCount > 0)
	{
		glBindBuffer(GL_ARRAY_BUFFER, vertexPositionVBO);
		glBufferSubData(GL_ARRAY_BUFFER, 0, vertexPositionData.getByteLength(), &vertexPositionData[0]);

		glBindBuffer(GL_ARRAY_BUFFER, vertexColorVBO);
		glBufferSubData(GL_ARRAY_BUFFER, 0, vertexColorData.getByteLength(), &vertexColorData[0]);

		glBindBuffer(GL_ARRAY_BUFFER, texCoordsVBO);
		glBufferSubData(GL_ARRAY_BUFFER, 0, texCoordsData.getByteLength(), &texCoordsData[0]);

		glDrawArrays(currentGLDrawMode, 0, currentVertexCount);
		vertexPositionData.clear();
		vertexColorData.clear();
		texCoordsData.clear();
		currentVertexCount = 0;
	}
}

FLX_FORCEINLINE void FlxCanvas2D::flipBuffer()
{
	SDL_GL_SwapWindow(sdlWindow);
}

FLX_FORCEINLINE float32 FlxCanvas2D::getWidth()
{
	return width;
}

FLX_FORCEINLINE float32 FlxCanvas2D::getHeight()
{
	return height;
}

FLX_FORCEINLINE void FlxCanvas2D::pushBatch(GLsizei vertexCount, GLenum drawModeGL, FlxDrawMode drawMode, FlxImage texture)
{
	GLuint tx0 = texture.getTexture2D();
	GLuint tx1 = currentTexture.getTexture2D();
	if (currentVertexCount + vertexCount >= FLX_MAX_VERTEICES ||
		drawMode != currentDrawMode ||
		drawModeGL != currentGLDrawMode ||
		tx0 != tx1)
	{
		flush();
		currentVertexCount = 0;
		currentGLDrawMode = drawModeGL;
		if (drawMode != currentDrawMode ||
			tx0 != tx1)
		{
			if (drawMode == TEXTURE)
			{
				textureShader.enable();
				currentTexture = texture;
				// TODO: Add support for multi-texture sample.
				glActiveTexture(texture.getTextureID());
				glBindTexture(GL_TEXTURE_2D, currentTexture.getTexture2D());
			}
			else
			{
				primitiveShader.enable();
			}
			currentDrawMode = drawMode;
		}
	}
	currentVertexCount += vertexCount;
}
