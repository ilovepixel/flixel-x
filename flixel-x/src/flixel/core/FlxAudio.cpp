#include <flixel/core/FlxAudio.h>
#include <flixel/core/FlxError.h>

FLX_FORCEINLINE static void checkALError()
{
	switch (alGetError())
	{
		case AL_INVALID_NAME:
			FlxError("OpenAL: Invalide Source Name");
			break;
		case AL_INVALID_ENUM:
			FlxError("OpenAL: Invalid Property ID");
			break;
		case AL_INVALID_VALUE:
			FlxError("OpenAL: Invalid Volume or Frequency");
			break;
		case AL_INVALID_OPERATION:
			FlxError("OpenAL: Invalid Source Operation");
			break;
		case AL_OUT_OF_MEMORY:
			FlxError("OpenAL: Out of Memory");
			break;
	};
}

static FLX_FORCEINLINE ALenum getALFormat(FlxWaveData* pWaveData)
{
	bool stereo = pWaveData->numChannels > 1;
	if (pWaveData->bitsPerSample == 16)
	{
		return stereo ? AL_FORMAT_STEREO16 : AL_FORMAT_MONO16;
	}
	else if (pWaveData->bitsPerSample == 8)
	{
		return stereo ? AL_FORMAT_STEREO8 : AL_FORMAT_MONO8;
	}
	return -1;
}

FlxAudio::FlxAudio() :
	sourceId(0xDEADBEEF),
	bufferId(0xDEADBEEF),
	waveData(nullptr),
	state(NOT_LOADED)
{}

bool FlxAudio::isLoaded()
{
	return state == LOADED;
}

void FlxAudio::setPitch(float32 pitch)
{
	alSourcef(sourceId, AL_PITCH, pitch);
	checkALError();
	this->pitch = pitch;
}

void FlxAudio::setGain(float32 gain)
{
	alSourcef(sourceId, AL_GAIN, gain);
	checkALError();
	this->pitch = gain;
}

void FlxAudio::setLoop(bool loop)
{
	alSourcei(sourceId, AL_LOOPING, loop ? AL_TRUE : AL_FALSE);
	checkALError();
	this->loop = loop;
}

float32 FlxAudio::getPitch()
{
	return pitch;
}

float32 FlxAudio::getGain()
{
	return gain;
}

bool FlxAudio::getLoop()
{
	return loop;
}

FlxAudio FlxAudio::loadAudio(const char* path, bool loop, float32 pitch, float32 gain)
{
	FlxAudio audioSource;
	FlxWaveData* waveData = FlxWaveData::loadWAV(path);
	alGenBuffers(1, &audioSource.bufferId);
	checkALError();
	alGenSources(1, &audioSource.sourceId);
	checkALError();
	if (audioSource.bufferId == 0xDEADBEEF)
	{
		FlxError("Audio context not initialized.");
	}	
	alSourcef(audioSource.sourceId, AL_PITCH, pitch);
	checkALError();
	alSourcef(audioSource.sourceId, AL_GAIN, gain);
	checkALError();
	alSource3f(audioSource.sourceId, AL_POSITION, 0, 0, 0);
	checkALError();
	alSource3f(audioSource.sourceId, AL_VELOCITY, 0, 0, 0);
	checkALError();
	alSourcei(audioSource.sourceId, AL_LOOPING, loop ? AL_TRUE : AL_FALSE);
	checkALError();
	alBufferData(audioSource.bufferId, getALFormat(waveData), waveData->data, waveData->subchunk2Size, waveData->sampleRate);
	checkALError();
	alSourcei(audioSource.sourceId, AL_BUFFER, audioSource.bufferId);
	checkALError();
	audioSource.loop = loop;
	audioSource.gain = gain;
	audioSource.pitch = pitch;
	audioSource.waveData = waveData;
	audioSource.state = FlxAudio::LOADED;
	return audioSource;
}

void FlxAudio::discardAudio(FlxAudio audio)
{
	alDeleteSources(1, &audio.sourceId);
	checkALError();
	alDeleteBuffers(1, &audio.bufferId);
	checkALError();
}
