#include <flixel/FlxBasic.h>
#include <flixel/FlxCamera.h>
#include <flixel/FlxG.h>

uint32 FlxBasic::_ACTIVECOUNT = 0;
uint32 FlxBasic::_VISIBLECOUNT = 0;

FlxBasic::FlxBasic() :
	ID(-1),
	exists(true), 
	active(true), 
	visible(true), 
	alive(true), 
	ignoreDrawDebug(false), 
	cameras(nullptr)
{}

void FlxBasic::preUpdate()
{
	++FlxBasic::_ACTIVECOUNT;
}

void FlxBasic::draw()
{
	if (cameras == nullptr)
	{
		cameras = &FlxG::cameras;
	}
	FlxCamera* camera = nullptr;
	int32 i = 0;
	int32 l = (int32)cameras->getLength();
	while (i < l)
	{
		camera = cameras->get(i);
		++FlxBasic::_VISIBLECOUNT;
		if (FlxG::visualDebug && !ignoreDrawDebug)
		{
			drawDebug(camera);
		}
	}
}

void FlxBasic::kill()
{
	alive = false;
	exists = false;
}

void FlxBasic::revive()
{
	alive = true;
	exists = true;
}

FlxBasic::FlxBasicType FlxBasic::getType()
{
	return FlxBasic::FLXBASIC;
}
