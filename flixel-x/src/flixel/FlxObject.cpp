#include <flixel/FlxObject.h>
#include <flixel/FlxU.h>
#include <flixel/FlxGroup.h>
#include <flixel/FlxCamera.h>
#include <flixel/FlxG.h>
#include <math.h>

FlxObject::FlxObject(float32 x, float32 y, float32 width, float32 height) :
	FlxBasic(),
	x(x),
	y(y),
	width(width),
	height(height),
	last(x, y),
	mass(1.0f),
	elasticity(0.0f),
	immovable(false),
	moves(true),
	touching(NONE),
	wasTouching(NONE),
	allowCollisions(ANY),
	velocity(),
	acceleration(),
	drag(),
	maxVelocity(10000, 10000),
	angle(0),
	angularVelocity(0),
	angularAcceleration(0),
	angularDrag(0),
	maxAngular(10000),
	scrollFactor(1.0f, 1.0f),
	_flicker(false),
	_flickerTimer(0),
	_point(),
	_rect(),
	//path(nullptr),
	pathSpeed(0),
	pathAngle(0)
{}

void FlxObject::destroy()
{}

void FlxObject::preUpdate()
{
	++FlxBasic::_ACTIVECOUNT;
	if (_flickerTimer != 0)
	{
		if (_flickerTimer > 0)
		{
			_flickerTimer = _flickerTimer - FlxG::elapsed;
			if (_flickerTimer <= 0)
			{
				_flickerTimer = 0;
				_flicker = false;
			}
		}
	}
	last.x = x;
	last.y = y;
}

void FlxObject::postUpdate()
{
	if (moves)
	{
		updateMotion();
	}
	wasTouching = touching;
	touching = NONE;
}

void FlxObject::draw()
{
	if (cameras == nullptr)
	{
		cameras = &FlxG::cameras;
	}
	FlxCamera* camera = nullptr;
	uint32 i = 0;
	uint32 l = (uint32)cameras->getLength();
	while (i < l)
	{
		camera = cameras->get(i);
		if (!onScreen(camera))
		{
			continue;
		}
		++FlxBasic::_VISIBLECOUNT;
		if (FlxG::visualDebug && !ignoreDrawDebug)
		{
			drawDebug(camera);
		}
	}
}

void FlxObject::drawDebug(FlxCamera* camera)
{}

bool FlxObject::overlaps(FlxBasic* objectOrGroup, bool inScreenSpace, FlxCamera* camera)
{
	if (objectOrGroup->getType() == FlxBasic::FLXGROUP)
	{
		FlxGroup* group = FlxCast<FlxGroup>(objectOrGroup);
		bool result = false;
		size_t i = 0;
		FlxArray<FlxBasic*>& members = group->members;
		while (i < members.getLength())
		{
			if (overlaps(members[i++], inScreenSpace, camera))
			{
				result = true;
			}
		}
		return result;
	}
	if (objectOrGroup->getType() == FlxBasic::FLXTILEMAP)
	{
		return false;
	}
	FlxObject* object = FlxCast<FlxObject>(objectOrGroup);
	if (!inScreenSpace)
	{
		return (object->x + object->width > x) && (object->x < x + width) &&
			(object->y + object->height > y) && (object->y < y + height);
	}
	if (camera == nullptr)
	{
		camera = FlxG::camera;
	}
	FlxPoint objectScreenPos = object->getScreenXY(camera);
	_point = getScreenXY(camera);
	return	(objectScreenPos.x + object->width > _point.x) && (objectScreenPos.x < _point.x + width) &&
		(objectScreenPos.y + object->height > _point.y) && (objectScreenPos.y < _point.y + height);
}

bool FlxObject::overlapsAt(float32 x, float32 y, FlxBasic* objectOrGroup, bool inScreenSpace, FlxCamera* camera)
{
	return false;
}

bool FlxObject::overlapsPoint(FlxPoint& point, bool inScreenSpace, FlxCamera* camera)
{
	return false;
}

bool FlxObject::onScreen(FlxCamera* camera)
{
	if (camera == nullptr)
	{
		camera = FlxG::camera;
	}
	_point = getScreenXY(camera);
	return (_point.x + width > 0) && 
		(_point.x < camera->width) && 
		(_point.y + height > 0) && 
		(_point.y < camera->height);
}

FlxPoint FlxObject::getScreenXY(FlxCamera* camera)
{
	FlxPoint point;
	if (camera == nullptr)
	{
		camera = FlxG::camera;
	}
	point.x = x - (camera->scroll.x * scrollFactor.x);
	point.y = y - (camera->scroll.y * scrollFactor.y);
	point.x += (point.x > 0) ? 0.0000001f : -0.0000001f;
	point.y += (point.y > 0) ? 0.0000001f : -0.0000001f;
	return point;
}

void FlxObject::flicker(float32 duration)
{
	_flickerTimer = duration;
	if (_flickerTimer == 0)
	{
		_flicker = false;
	}
}

bool FlxObject::isFlickering()
{
	return _flickerTimer != 0;
}

bool FlxObject::isSolid()
{
	return (allowCollisions & ANY) > NONE;
}

void FlxObject::setSolid(bool solid)
{
	allowCollisions = solid ? ANY : NONE;
}

FlxPoint FlxObject::getMidpoint()
{
	FlxPoint point(x + width * 0.5f, y + height * 0.5f);
	return point;
}

void FlxObject::reset(float32 x, float32 y)
{
	revive();
	touching = NONE;
	wasTouching = NONE;
	this->x = x;
	this->y = y;
	last.x = x;
	last.y = y;
	velocity.x = 0;
	velocity.y = 0;
}

bool FlxObject::isTouching(uint32 direction)
{
	return (touching & direction) > NONE;
}

bool FlxObject::justTouched(uint32 direction)
{
	return (touching & direction) > NONE && (wasTouching & direction) <= NONE;
}

void FlxObject::hurt(float32 damage)
{
	health -= damage;
	if (health <= 0)
	{
		kill();
	}
}

bool FlxObject::separate(FlxObject* object1, FlxObject* object2)
{
	bool separatedX = separateX(object1, object2);
	bool separatedY = separateY(object1, object2);
	return separatedX || separatedY;
}

bool FlxObject::separateX(FlxObject* object1, FlxObject* object2)
{
	bool obj1immovable = object1->immovable;
	bool obj2immovable = object2->immovable;
	if (obj1immovable && obj2immovable)
	{
		return false;
	}
	if (object1->getType() == FlxBasic::FLXTILEMAP)
	{
		return false;
	}
	if (object2->getType() == FlxBasic::FLXTILEMAP)
	{
		return false;
	}
	float32 overlap = 0;
	float32 obj1delta = object1->x - object1->last.x;
	float32 obj2delta = object2->x - object2->last.x;
	if (obj1delta != obj2delta)
	{
		float32 obj1deltaAbs = (obj1delta > 0) ? obj1delta : -obj1delta;
		float32 obj2deltaAbs = (obj2delta > 0) ? obj2delta : -obj2delta;
		FlxRect obj1rect(
			object1->x - (obj1delta > 0 ? obj1delta : 0),
			object1->last.y,
			object1->width + obj1deltaAbs,
			object1->height
			);
		FlxRect obj2rect(
			object2->x - (obj2delta > 0 ? obj2delta : 0),
			object2->last.y,
			object2->width + obj2deltaAbs,
			object2->height
			);
		if ((obj1rect.x + obj1rect.width > obj2rect.x) &&
			(obj1rect.x < obj2rect.x + obj2rect.width) &&
			(obj1rect.y + obj1rect.height > obj2rect.y) &&
			(obj1rect.y < obj2rect.y + obj2rect.height))
		{
			float32 maxOverlap = obj1deltaAbs + obj2deltaAbs + OVERLAP_BIAS;
			if (obj1delta > obj2delta)
			{
				overlap = object1->x + object1->width - object2->x;
				if (overlap > maxOverlap || !(object1->allowCollisions & RIGHT) || !(object2->allowCollisions & LEFT))
				{
					overlap = 0;
				}
				else
				{
					object1->touching |= RIGHT;
					object2->touching |= LEFT;
				}
			}
			else if (obj1delta < obj2delta)
			{
				overlap = object1->x - object2->width - object2->x;
				if (-overlap > maxOverlap || !(object1->allowCollisions & LEFT) || !(object2->allowCollisions & RIGHT))
				{
					overlap = 0;
				}
				else
				{
					object1->touching |= LEFT;
					object2->touching |= RIGHT;
				}
			}
		}
	}
	if (overlap != 0)
	{
		float32 obj1v = object1->velocity.x;
		float32 obj2v = object2->velocity.x;

		if (!obj1immovable && !obj2immovable)
		{
			overlap *= 0.5;
			object1->x = object1->x - overlap;
			object2->x += overlap;

			float32 obj1velocity = sqrtf((obj2v * obj2v * object2->mass) / object1->mass) * ((obj2v > 0) ? 1 : -1);
			float32 obj2velocity = sqrtf((obj1v * obj1v * object1->mass) / object2->mass) * ((obj1v > 0) ? 1 : -1);
			float32 average = (obj1velocity + obj2velocity) * 0.5f;
			obj1velocity -= average;
			obj2velocity -= average;
			object1->velocity.x = average + obj1velocity * object1->elasticity;
			object2->velocity.x = average + obj2velocity * object2->elasticity;
		}
		else if (!obj1immovable)
		{
			object1->x = object1->x - overlap;
			object1->velocity.x = obj2v - obj1v*object1->elasticity;
		}
		else if (!obj2immovable)
		{
			object2->x += overlap;
			object2->velocity.x = obj1v - obj2v*object2->elasticity;
		}
		return true;
	}
	return false;
}

bool FlxObject::separateY(FlxObject* object1, FlxObject* object2)
{
	bool obj1immovable = object1->immovable;
	bool obj2immovable = object2->immovable;
	if (obj1immovable && obj2immovable)
	{
		return false;
	}
	if (object1->getType() == FlxBasic::FLXTILEMAP)
	{
		return false;
	}
	if (object2->getType() == FlxBasic::FLXTILEMAP)
	{
		return false;
	}
	float32 overlap = 0;
	float32 obj1delta = object1->y - object1->last.y;
	float32 obj2delta = object2->y - object2->last.y;
	if (obj1delta != obj2delta)
	{
		float32 obj1deltaAbs = (obj1delta > 0) ? obj1delta : -obj1delta;
		float32 obj2deltaAbs = (obj2delta > 0) ? obj2delta : -obj2delta;
		FlxRect obj1rect(
			object1->x,
			object1->y - (obj1delta > 0 ? obj1delta : 0),
			object1->width,
			object1->height + obj1deltaAbs
			);
		FlxRect obj2rect(
			object2->x,
			object2->y - (obj2delta > 0 ? obj2delta : 0),
			object2->width,
			object2->height + obj2deltaAbs
			);
		if ((obj1rect.x + obj1rect.width > obj2rect.x) &&
			(obj1rect.x < obj2rect.x + obj2rect.width) &&
			(obj1rect.y + obj1rect.height > obj2rect.y) &&
			(obj1rect.y < obj2rect.y + obj2rect.height))
		{
			float32 maxOverlap = obj1deltaAbs + obj2deltaAbs + OVERLAP_BIAS;
			if (obj1delta > obj2delta)
			{
				overlap = object1->y + object1->height - object2->y;
				if (overlap > maxOverlap || !(object1->allowCollisions & DOWN) || !(object2->allowCollisions & UP))
				{
					overlap = 0;
				}
				else
				{
					object1->touching |= DOWN;
					object2->touching |= UP;
				}
			}
			else if (obj1delta < obj2delta)
			{
				overlap = object1->y - object2->height - object2->y;
				if (-overlap > maxOverlap || !(object1->allowCollisions & UP) || !(object2->allowCollisions & DOWN))
				{
					overlap = 0;
				}
				else
				{
					object1->touching |= UP;
					object2->touching |= DOWN;
				}
			}
		}
	}
	if (overlap != 0)
	{
		float32 obj1v = object1->velocity.y;
		float32 obj2v = object2->velocity.y;

		if (!obj1immovable && !obj2immovable)
		{
			overlap *= 0.5;
			object1->x = object1->x - overlap;
			object2->x += overlap;

			float32 obj1velocity = sqrtf((obj2v * obj2v * object2->mass) / object1->mass) * ((obj2v > 0) ? 1 : -1);
			float32 obj2velocity = sqrtf((obj1v * obj1v * object1->mass) / object2->mass) * ((obj1v > 0) ? 1 : -1);
			float32 average = (obj1velocity + obj2velocity) * 0.5f;
			obj1velocity -= average;
			obj2velocity -= average;
			object1->velocity.y = average + obj1velocity * object1->elasticity;
			object2->velocity.y = average + obj2velocity * object2->elasticity;
		}
		else if (!obj1immovable)
		{
			object1->y = object1->y - overlap;
			object1->velocity.y = obj2v - obj1v*object1->elasticity;
		}
		else if (!obj2immovable)
		{
			object2->y += overlap;
			object2->velocity.y = obj1v - obj2v*object2->elasticity;
		}
		return true;
	}
	return false;
}

FlxBasic::FlxBasicType FlxObject::getType()
{
	return FlxBasic::FLXOBJECT;
}

void FlxObject::updateMotion()
{
	float32 delta = 0;
	float32 velocityDelta = 0;
	velocityDelta = (FlxU::computeVelocity(angularVelocity, angularAcceleration, angularDrag, maxAngular) - angularVelocity) / 2;
	angularVelocity += velocityDelta;
	angle += angularVelocity * FlxG::elapsed;
	angularVelocity += velocityDelta;

	velocityDelta = (FlxU::computeVelocity(velocity.x, acceleration.x, drag.x, maxVelocity.x) - velocity.x) / 2;
	velocity.x += velocityDelta;
	delta = velocity.x * FlxG::elapsed;
	velocity.x += velocityDelta;
	x += delta;

	velocityDelta = (FlxU::computeVelocity(velocity.y, acceleration.y, drag.y, maxVelocity.y) - velocity.y) / 2;
	velocity.y += velocityDelta;
	delta = velocity.y * FlxG::elapsed;
	velocity.y += velocityDelta;
	y += delta;
}
