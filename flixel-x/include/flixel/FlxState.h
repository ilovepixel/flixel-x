#pragma once

#include <flixel/core/FlxCore.h>
#include <flixel/FlxGroup.h>

struct FLX_API FlxState : public FlxGroup, public FlxClass
{
	static FlxState* Create() 
	{
		return new FlxState();
	}
	static void Destroy(FlxState* object) 
	{
		object->destroy();
		delete object;
	}
	FlxState() :
		FlxGroup()
	{}
	virtual void create() {}
};