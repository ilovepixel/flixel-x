#pragma once

#include <flixel/core/FlxCore.h>
#include <flixel/FlxPoint.h>
#include <stdlib.h>

struct FlxU
{
	static FLX_API float32 abs(float32 value);
	static FLX_API float32 floor(float32 value);
	static FLX_API float32 ceil(float32 value);
	static FLX_API float32 round(float32 value);
	static FLX_API float32 min(float32 numberA, float32 numberB);
	static FLX_API float32 max(float32 numberA, float32 numberB);
	static FLX_API float32 bound(float32 value, float32 min, float32 max);
	static FLX_API int32 srand(int32 seed);
	static FLX_API uint32 getTicks();
	static FLX_API void formatTicks(uint32 startTicks, uint32 endTicks, FlxString& output);
	static FLX_API uint32 makeColor(uint8 red, uint8 green, uint8 blue, uint8 alpha);
	static FLX_API void getRGBA(uint32 color, uint8* output);
	static FLX_API void formatTime(int32 seconds, bool showMS, FlxString& output);
	static FLX_API float32 computeVelocity(float32 velocity, float32 acceleration = 0.0, float32 drag = 0.0, float32 max = 10000.0);
	static FLX_API void rotatePoint(float32 x, float32 y, float32 pivotX, float32 pivotY, float32 angle, FlxPoint& point);
	static FLX_API float32 getAngle(FlxPoint point1, FlxPoint point2);
	static FLX_API float32 getDistance(FlxPoint point1, FlxPoint point2);

	// Templated functions
	template<class T>
	static void shuffle(T* objects, size_t len, uint32 howManyTimes)
	{
		uint32 i = 0;
		uint32 index1 = 0;
		uint32 index2 = 0;
		T* object = nullptr;
		while (i < howManyTimes)
		{
			index1 = rand() % len;
			index2 = rand() % len;
			object = &objects[index2];
			objects[index2] = objects[index1];
			objects[index1] = *object;
			++i;
		}
	}
	template<class T>
	static T& getRandom(T* objects, size_t len, size_t startIndex, size_t length)
	{
		size_t l = len;
		if (l == 0 || l > len - startIndex)
		{
			l = len - startIndex;
		}
		return objects[startIndex + (rand() % l)];
	}
};