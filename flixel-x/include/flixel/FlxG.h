#pragma once

#include <flixel/core/FlxCore.h>
#include <flixel/FlxGame.h>
#include <flixel/FlxRect.h>
#include <flixel/FlxGroup.h>
#include <flixel/FlxCamera.h>
#include <flixel/FlxSound.h>

template class FLX_API FlxArray<FlxCamera*>;

class FLX_API FlxG
{
public:
	typedef std::function<bool(FlxObject*, FlxObject*)> FlxCollisionOverlap;

	static const char* getLibraryName();
	static uint32 getFramerate();
	static void setFramerate(uint32 framerate);
	static float32 random();
	static void resetState();
	static void resetGame();
	static void resetInput();
	static void playMusic(FlxAudio music, float32 volume = 1.0f);
	static void playSound(FlxAudio sound, float32 volume = 1.0f, bool looped = false);
	static float32 getVolume();
	static void setVolume(float32 volume);
	static void destroySounds(bool forceDestroy = false);
	static void updateSounds();
	static void pauseSounds();
	static void resumeSounds();
	static FlxImage createBitmap(float32 width, float32 height, uint32 color, bool unique = false, const char* key = nullptr);
	static FlxImage addBitmap(FlxImage graphic, bool reverse = false, bool unique = false, const char* key = nullptr);
	static FlxState* getState();
	static void switchState(FlxState* state);
	static void addCamera(FlxCamera* camera);
	static void removeCamera(FlxCamera* camera, bool destroy = true);
	static void resetCameras(FlxCamera* newCamera = nullptr);
	static void flash(uint32 color = 0xFFFFFFFF, float32 duration = 1.0f, bool force = false);
	static void fade(uint32 color = 0xFF000000, float32 duration = 1.0f, bool force = false);
	static void shake(float32 intensity = 0.05f, float32 duration = 0.5f, bool force = false, uint32 direction = 0);
	static uint32 getBgColor();
	static void setBgColor(uint32 color);
	static bool overlap(FlxBasic* objectOrGroup1 = nullptr, FlxBasic* objectOrGroup2 = nullptr, FlxCollisionOverlap notifyCallback = nullptr, FlxCollisionOverlap processCallback = nullptr);
	static bool collide(FlxBasic* objectOrGroup1 = nullptr, FlxBasic* objectOrGroup2 = nullptr, FlxCollisionOverlap notifyCallback = nullptr);
	static void init(FlxGame* game, float32 width, float32 height, float32 zoom);
	static void reset();
	static void updateInput();
	static void lockCameras();
	static void unlockCameras();
	static void updateCameras();
	static void destroy();

	// Templated functions
	template<class T>
	static void shuffle(T* objects, size_t len, uint32 howManyTimes)
	{
		uint32 i = 0;
		uint32 index1 = 0;
		uint32 index2 = 0;
		T* object = nullptr;
		while (i < howManyTimes)
		{
			index1 = (uint32)(FlxG::random() * len);
			index2 = (uint32)(FlxG::random() * len);
			object = &objects[index2];
			objects[index2] = objects[index1];
			objects[index1] = *object;
			++i;
		}
	}
	template<class T>
	static T& getRandom(T* objects, size_t len, size_t startIndex, size_t length)
	{
		size_t l = len;
		if (l == 0 || l > len - startIndex)
		{
			l = len - startIndex;
		}
		return objects[startIndex + ((uint32)(FlxG::random() * len))];
	}

public:
	static const char* FlxG::LIBRARY_NAME;
	static const uint8 FlxG::LIBRARY_MAJOR_VERSION = 1;
	static const uint8 FlxG::LIBRARY_MINOR_VERSION = 0;
	static const uint8 FlxG::DEBUGGER_STANDARD = 0;
	static const uint8 FlxG::DEBUGGER_MICRO = 1;
	static const uint8 FlxG::DEBUGGER_BIG = 2;
	static const uint8 FlxG::DEBUGGER_TOP = 3;
	static const uint8 FlxG::DEBUGGER_LEFT = 4;
	static const uint8 FlxG::DEBUGGER_RIGHT = 5;
	static const uint32 FlxG::RED = 0xffff0012;
	static const uint32 FlxG::GREEN = 0xff00f225;
	static const uint32 FlxG::BLUE = 0xff0090e9;
	static const uint32 FlxG::PINK = 0xfff01eff;
	static const uint32 FlxG::WHITE = 0xffffffff;
	static const uint32 FlxG::BLACK = 0xff000000;

	static bool paused;
	static bool debug;
	static float32 elapsed;
	static float32 timeScale;
	static float32 width;
	static float32 height;
	static FlxRect worldBounds;
	static uint32 worldDivisions;
	static bool visualDebug;
	static bool mobile;
	static int32 globalSeed;
	static int32 level;
	static float32 score;
	//static FlxArray<FlxSave> saves;
	static uint32 save;
	static FlxMouse mouse;
	static FlxKeyboard keyboard;
	static FlxSound music;
	static FlxGroup sounds;
	static bool mute;
	static FlxStaticArray<FlxCamera*, 100> cameras;
	static FlxCamera* camera;
	static FlxCanvas2D* canvas;
	static FlxAudioPlayer* audioPlayer;

protected:

	static float32 _volume;
	static FlxGame* _game;

};