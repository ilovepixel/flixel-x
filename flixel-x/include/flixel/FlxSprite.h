#pragma once

#include <flixel/core/FlxCore.h>
#include <flixel/FlxObject.h>
#include <flixel/FlxPoint.h>
#include <flixel/system/FlxAnim.h>

template class FLX_API FlxArray<FlxAnim*>;

class FLX_API FlxSprite : public FlxObject
{
public:
	FlxSprite(float32 x = 0, float32 y = 0, FlxImage graphic = FlxImage::loadEmbeddedImage((void*)FlxEmbeddedAssets::DEFAULT_PIXELS, 16, 16));
	void destroy();
	void loadGraphic(FlxImage graphic, bool animated = false, bool reverse = false, float32 width = 0, float32 height = 0, bool unique = false);
	void makeGraphic(float32 width, float32 height, uint32 color, bool unique = false);
	void resetHelpers();
	virtual void postUpdate() override;
	virtual void draw() override;
	void fill(uint32 color);
	void updateAnimation();
	void drawFrame(bool force = false);
	void addAnimation(char* name, FlxArray<uint32> frames, float32 frameRate = 0, bool looped = true);
	void play(char* animName, bool force = false);
	void randomFrame();
	void setOriginToCorner();
	void centerOffsets(bool adjustPosition = false);
	FlxImage getPixels();
	void setPixels(FlxImage pixels);
	uint32 getFacing();
	void setFacing(uint32 direction);
	float32 getAlpha();
	void setAlpha(float32 alpha);
	uint32 getColor();
	void setColor(uint32 color);
	uint32 getFrame();
	void setFrame(uint32 frame);
	virtual bool onScreen(FlxCamera* camera) override;
	void calcFrame();

public:
	FlxPoint origin;
	FlxPoint offset;
	FlxPoint scale;
	bool antialiasing;
	bool finished;
	float32 frameWidth;
	float32 frameHeight;
	uint32 frames;
	bool dirty;

private:
	FlxArray<FlxAnim*> _animations;
	uint32 _flipped;
	FlxAnim* _curAnim;
	uint32 _curFrame;
	uint32 _curIndex;
	float32 _frameTimer;
	uint32 _facing;
	float32 _alpha;
	uint32 _color;
	FlxImage _pixels;
	FlxPoint _framePos;
	uint8 _tempColorArray[4];
};