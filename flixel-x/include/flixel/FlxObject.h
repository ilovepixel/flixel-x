#pragma once

#include <flixel/core/FlxCore.h>
#include <flixel/FlxBasic.h>
#include <flixel/FlxPoint.h>
#include <flixel/FlxRect.h>

class FLX_API FlxObject : public FlxBasic
{
public:
	static FlxObject* Create(float32 x = 0, float32 y = 0, float32 width = 0, float32 height = 0)
	{
		return new FlxObject(x, y, width, height);
	}
	static void Destroy(FlxObject* object)
	{
		object->destroy();
		delete object;
	}
	FlxObject(float32 x = 0, float32 y = 0, float32 width = 0, float32 height = 0);
	virtual void destroy() override;
	virtual void preUpdate() override;
	virtual void postUpdate() override;
	virtual void draw() override;
	virtual void drawDebug(FlxCamera* camera = nullptr) override;
	//void followPath(FlxPath& path, float32 speed = 100, FlxPathMode mode = FlxPathMode::PATH_FORWARD, bool autoRotate = false);
	//void stopFollowingPath(bool destroyPath = false);
	//FlxPoint& advancePath(bool snap = true);
	bool overlaps(FlxBasic* objectOrGroup, bool inScreenSpace = false, FlxCamera* camera = nullptr);
	bool overlapsAt(float32 x, float32 y, FlxBasic* objectOrGroup, bool inScreenSpace = false, FlxCamera* camera = nullptr);
	bool overlapsPoint(FlxPoint& point, bool inScreenSpace = false, FlxCamera* camera = nullptr);
	virtual bool onScreen(FlxCamera* FlxCamera = nullptr);
	FlxPoint getScreenXY(FlxCamera* camera = nullptr);
	void flicker(float32 duration = 1.0f);
	bool isFlickering();
	bool isSolid();
	void setSolid(bool solid);
	FlxPoint getMidpoint();
	void reset(float32 x, float32 y);
	bool isTouching(uint32 direction);
	bool justTouched(uint32 direction);
	void hurt(float32 damage);
	static bool separate(FlxObject* object1, FlxObject* object2);
	static bool separateX(FlxObject* object1, FlxObject* object2);
	static bool separateY(FlxObject* object1, FlxObject* object2);
	virtual FlxBasicType getType();
protected:
	void updateMotion();
	//void udpatePathMotion();

public:
	static const uint32 LEFT = 0x0001;
	static const uint32 RIGHT = 0x0010;
	static const uint32 UP = 0x0100;
	static const uint32 DOWN = 0x1000;
	static const uint32 NONE = 0x0000;
	static const uint32 CEILING = UP;
	static const uint32 FLOOR = DOWN;
	static const uint32 WALL = LEFT | RIGHT;
	static const uint32 ANY = LEFT | RIGHT | UP | DOWN;
	static const int32 OVERLAP_BIAS = 4;
	static const uint32 PATH_FORWARD = 0x000000;
	static const uint32 PATH_BACKWARD = 0x000001;
	static const uint32 PATH_LOOP_FORWARD = 0x000010;
	static const uint32 PATH_LOOP_BACKWARD = 0x000100;
	static const uint32 PATH_YOYO = 0x001000;
	static const uint32 PATH_HORIZONTAL_ONLY = 0x010000;
	static const uint32 PATH_VERTICAL_ONLY = 0x100000;

	float32 x;
	float32 y;
	float32 width;
	float32 height;
	bool immovable;
	FlxPoint velocity;
	float32 mass;
	float32 elasticity;
	FlxPoint acceleration;
	FlxPoint drag;
	FlxPoint maxVelocity;
	float32 angle;
	float32 angularVelocity;
	float32 angularAcceleration;
	float32 angularDrag;
	float32 maxAngular;
	FlxPoint scrollFactor;
	float32 health;
	bool moves;
	uint32 touching;
	uint32 wasTouching;
	uint32 allowCollisions;
	FlxPoint last;
	//FlxPath path;
	float32 pathSpeed;
	float32 pathAngle;

protected:
	int32 _pathNodeIndex;
	uint32 _pathMode;
	int32 _pathInc;
	bool _pathRotate;
	FlxRect _rect;
	FlxPoint _point;
	bool _flicker;
	float32 _flickerTimer;
	static const FlxPoint _pZero;
};
