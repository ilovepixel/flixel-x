#pragma once

#include <flixel/core/FlxCore.h>
#include <flixel/FlxBasic.h>

template class FLX_API FlxArray<FlxBasic*>;

class FLX_API FlxGroup : public FlxBasic
{
public:
	enum FlxSortType
	{
		ASCENDING = -1,
		DESCENDING = 1
	};
	static FlxGroup* Create(size_t maxSize = 0)
	{
		return new FlxGroup(maxSize);
	}
	static void Destroy(FlxGroup* object)
	{
		object->destroy();
		delete object;
	}
	FlxGroup(size_t maxSize = 0);
	virtual void destroy() override;
	virtual void preUpdate() override {};
	virtual void update() override;
	virtual void draw() override;
	virtual void kill() override;
	size_t getMaxSize();
	void setMaxSize(size_t size);
	void add(FlxBasic* object);
	void remove(FlxBasic* object, bool splice = false);
	void replace(FlxBasic* oldObject, FlxBasic* newObject);
	FlxBasic* getFirstExtant();
	FlxBasic* getFirstAlive();
	FlxBasic* getFirstDead();
	size_t countLiving();
	size_t countDead();
	FlxBasic* getRandom(size_t startIndex, size_t length);
	void clear();
	virtual FlxBasicType getType();

public:
	size_t length;
	FlxArray<FlxBasic*> members;

protected:
	size_t _maxSize;
	size_t _marker;
	size_t _sortIndex;
	size_t _sortOrder;
};

