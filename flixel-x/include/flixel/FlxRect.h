#pragma once

#include <flixel/core/FlxCore.h>

struct FLX_API FlxRect
{
	static FlxRect* Create(float32 x = 0, float32 y = 0, float32 width = 0, float32 height = 0)
	{
		return new FlxRect(x, y, width, height);
	}
	static void Destroy(FlxRect* object)
	{
		delete object;
	}
	FlxRect(float32 x = 0, float32 y = 0, float32 width = 0, float32 height = 0);
	FlxRect(FlxRect& rect);
	bool isZero();
	void zeroOut();
	float32 getLeft();
	float32 getRight();
	float32 getTop();
	float32 getBottom();
	FlxRect& make(float32 x, float32 y, float32 width, float32 height);
	FlxRect& copyFrom(FlxRect& rect);
	FlxRect& copyTo(FlxRect& rect);
	bool overlaps(FlxRect& rect);
	float32 x;
	float32 y;
	float32 width;
	float32 height;
};