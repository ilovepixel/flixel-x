#pragma once

#include <flixel/core/FlxCore.h>
#include <flixel/FlxState.h>

class FLX_API FlxGame final
{
public:
	FlxGame(float32 gameSizeX, float32 gameSizeY, FlxClass::Type initialState, float32 zoom = 1.0f, uint32 gameFrameRate = 60, bool useSystemCursor = false);
	virtual ~FlxGame();
	void showSoundTray(bool silent = false);
	void onKeyUp(SDL_Event sdlEvent);
	void onKeyDown(SDL_Event sdlEvent);
	void onMouseDown(SDL_Event sdlEvent);
	void onMouseUp(SDL_Event sdlEvent);
	void onMouseWheel(SDL_Event sdlEvent);
	void onMouseMove(SDL_Event sdlEvent);
	void onFocus(SDL_Event sdlEvent);
	void onFocusLost(SDL_Event sdlEvent);
	void onEnterFrame();
	void switchState();
	void step();
	void updateSoundTray(uint32 MS);
	void update();
	void draw();
	void create();
	void createSoundTray();
	void createFocusScreen();
	void exitGame();

public:
	bool useSoundHotKeys;
	bool useSystemCursor;
	bool forceDebugger;
	float32 x;
	float32 y;
	FlxState* _state;
	FlxState* _requestedState;
	bool _requestedReset;
	uint32 _step;
	uint32 _maxAccumulation;

private:
	FlxImage _mouse;
	FlxClass::Type _iState;
	bool _created;
	uint32 _total;
	uint32 _accumulator;
	bool _lostFocus;
	float32 _soundTrayTimer;
	//FlxDebugger _debugger;
	bool _debuggerUp;
	//FlxReplay _replay;
	bool _replayRequested;
	bool _recrodingRequested;
	bool _replaying;
	bool _recording;
	int32 _replayTimer;
	bool _runGame;

};