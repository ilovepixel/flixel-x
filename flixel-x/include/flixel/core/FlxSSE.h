#pragma once

#include <flixel/core/FlxPlatform.h>
#include <flixel/core/FlxTypes.h>

#include <immintrin.h>

typedef __m128 FlxVectorReg;
namespace FlxSIMD
{
	FLX_FORCEINLINE FLX_API FlxVectorReg set(float32 x, float32 y, float32 z, float32 w)
	{
		return _mm_set_ps(x, y, z, w);
	}
	FLX_FORCEINLINE FLX_API FlxVectorReg setReverse(float32 x, float32 y, float32 z, float32 w)
	{
		return _mm_setr_ps(x, y, z, w);
	}
	FLX_FORCEINLINE FLX_API FlxVectorReg setZero()
	{
		return _mm_setzero_ps();
	}
	FLX_FORCEINLINE FLX_API FlxVectorReg setOne(float32 x)
	{
		return _mm_set_ps1(x);
	}
	FLX_FORCEINLINE FLX_API FlxVectorReg load(float32* scalar_vector)
	{
		return _mm_load_ps(scalar_vector);
	}
	FLX_FORCEINLINE FLX_API FlxVectorReg loadUnaligned(float32* scalar_vector)
	{
		return _mm_loadu_ps(scalar_vector);
	}
	FLX_FORCEINLINE FLX_API void store(float32* scalar_vector, FlxVectorReg dest)
	{
		_mm_store_ps(scalar_vector, dest);
	}
	FLX_FORCEINLINE FLX_API void storeUnaligned(float32* scalar_vector, FlxVectorReg dest)
	{
		_mm_storeu_ps(scalar_vector, dest);
	}
	FLX_FORCEINLINE FLX_API FlxVectorReg add(FlxVectorReg a, FlxVectorReg b)
	{
		return _mm_add_ps(a, b);
	}
	FLX_FORCEINLINE FLX_API FlxVectorReg sub(FlxVectorReg a, FlxVectorReg b)
	{
		return _mm_sub_ps(a, b);
	}
	FLX_FORCEINLINE FLX_API FlxVectorReg mul(FlxVectorReg a, FlxVectorReg b)
	{
		return _mm_mul_ps(a, b);
	}
	FLX_FORCEINLINE FLX_API FlxVectorReg min(FlxVectorReg a, FlxVectorReg b)
	{
		return _mm_min_ps(a, b);
	}
	FLX_FORCEINLINE FLX_API FlxVectorReg max(FlxVectorReg a, FlxVectorReg b)
	{
		return _mm_max_ps(a, b);
	}
	FLX_FORCEINLINE FLX_API FlxVectorReg and(FlxVectorReg a, FlxVectorReg b)
	{
		return _mm_and_ps(a, b);
	}
	FLX_FORCEINLINE FLX_API FlxVectorReg not(FlxVectorReg a, FlxVectorReg b)
	{
		return _mm_andnot_ps(a, b);
	}
	FLX_FORCEINLINE FLX_API FlxVectorReg or(FlxVectorReg a, FlxVectorReg b)
	{
		return _mm_or_ps(a, b);
	}
	FLX_FORCEINLINE FLX_API FlxVectorReg xor(FlxVectorReg a, FlxVectorReg b)
	{
		return _mm_xor_ps(a, b);
	}
	FLX_FORCEINLINE FLX_API FlxVectorReg equal(FlxVectorReg a, FlxVectorReg b)
	{
		return _mm_cmpeq_ps(a, b);
	}
	FLX_FORCEINLINE FLX_API FlxVectorReg lowerThan(FlxVectorReg a, FlxVectorReg b)
	{
		return _mm_cmplt_ps(a, b);
	}
	FLX_FORCEINLINE FLX_API FlxVectorReg lowerThanOrEqual(FlxVectorReg a, FlxVectorReg b)
	{
		return _mm_cmple_ps(a, b);
	}
	FLX_FORCEINLINE FLX_API FlxVectorReg greaterThan(FlxVectorReg a, FlxVectorReg b)
	{
		return _mm_cmpgt_ps(a, b);
	}
	FLX_FORCEINLINE FLX_API FlxVectorReg greaterThanOrEqual(FlxVectorReg a, FlxVectorReg b)
	{
		return _mm_cmpge_ps(a, b);
	}
}