#pragma once

#if defined(__ARM_NEON__)
#include <flixel/core/FlxNEON.h>
#elif defined(_M_X64) || defined(_M_IX86) || defined(__SSE__)
#include <flixel/core/FlxSSE.h>
#endif