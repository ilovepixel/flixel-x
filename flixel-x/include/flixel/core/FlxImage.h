#pragma once

#include <flixel/core/FlxPlatform.h>
#include <flixel/core/FlxTypes.h>
#include <flixel/core/FlxGL.h>

class FLX_API FlxImage
{
public:
	enum FlxImageFormat 
	{
		NONE,
		PNG,
		BMP,
		TGA
	};
	enum FlxImageLoadState
	{
		NOT_LOADED,
		LOADED,
		FAILED,
		IN_PROGRESS,
		DISCARDED
	};
public:
	FlxImage();
	FlxImage(float32 width, float32 height);
	FlxImage(float32 width, float32 height, uint32 color = 0xFFFFFFFF);
	bool isLoaded();
	const float32 getWidth();
	const float32 getHeight();
	const GLuint getTexture2D();
	const GLenum getTextureID();
	void readRGBAPixels(GLuint  x, GLuint y, GLsizei width, GLsizei height, uint8* buffer);

	static FlxImage loadImage(const char* path, FlxImageFormat format);
	static FlxImage loadEmbeddedImage(void* pixels, float32 width, float32 height);
	static void discardImage(FlxImage image);

	FlxImageLoadState state;
	GLuint ID;

protected:
	float32 width;
	float32 height;
	GLuint texture;
};