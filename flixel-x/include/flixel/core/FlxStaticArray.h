#pragma once

#include <flixel/core/FlxPlatform.h>
#include <flixel/core/FlxTypes.h>

template<class T, size_t Capacity, size_t Alignment = 4>
class FlxStaticArray
{
public:
	FlxStaticArray() :
		length(0)
	{}
	virtual ~FlxStaticArray()
	{
		for (size_t index = 0; index < length; ++index)
		{
			data[index].~T();
		}
	}
	FLX_FORCEINLINE void clearAndDestroy()
	{
		for (size_t index = 0; index < length; ++index)
		{
			data[index].~T();
		}
		length = 0;
	}
	FLX_FORCEINLINE void clear() 
	{
		length = 0;
	}
	FLX_FORCEINLINE void push(T& element)
	{
		data[length++] = element;
	}
	FLX_FORCEINLINE void push(T&& element)
	{
		data[length++] = element;
	}
	FLX_FORCEINLINE void pushBatch(T* elements, size_t len)
	{
		for (size_t index = 0; index < len; ++index)
		{
			data[length++] = elements[index];
		}
	}
	FLX_FORCEINLINE T& pop()
	{
		return data[--length];
	}
	FLX_FORCEINLINE size_t getLength()
	{
		return length;
	}
	FLX_FORCEINLINE size_t getByteLength()
	{
		return length * sizeof(T);
	}
	FLX_FORCEINLINE size_t getCapacity()
	{
		return Capacity;
	}
	FLX_FORCEINLINE T& operator[](size_t index)
	{
		return data[index];
	}
	FLX_FORCEINLINE T& get(size_t index)
	{
		return data[index];
	}
	FLX_FORCEINLINE virtual void fill(T&& value)
	{
		for (size_t index = 0; index < length; ++index)
		{
			data[index] = value;
		}
	}
	FLX_FORCEINLINE bool contains(T&& value)
	{
		for (size_t index = 0; index < length; ++index)
		{
			if (data[index] == value)
			{
				return true;
			}
		}
		return false;
	}
	FLX_FORCEINLINE bool contains(T& value)
	{
		for (size_t index = 0; index < length; ++index)
		{
			if (data[index] == value)
			{
				return true;
			}
		}
		return false;
	}
	FLX_FORCEINLINE int32 indexOf(T&& value)
	{
		for (int32 index = 0; index < (int32)length; ++index)
		{
			if (data[index] == value)
			{
				return index;
			}
		}
		return -1;
	}
	FLX_FORCEINLINE int32 indexOf(T& value)
	{
		for (int32 index = 0; index < (int32)length; ++index)
		{
			if (data[index] == value)
			{
				return index;
			}
		}
		return -1;
	}

protected:
	size_t length;
	FLX_ALIGN(Alignment) T data[Capacity];
};