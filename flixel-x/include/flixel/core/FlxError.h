#pragma once
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#define __NEWLINESPACE__ "\n  "
#define __NEWLINE__ "\n"
#define __SEP__ " : "
#define __STR2__(x) #x
#define __STR1__(x) __STR2__(x)
#define __LOC__ __TIMESTAMP__ __NEWLINE__ __FILE__ __SEP__ __STR1__(__LINE__) __NEWLINESPACE__

#if _DEBUG

#define FlxError(message, ...) {\
	fprintf(stderr, __LOC__); \
	fprintf(stderr, "\nFlxError: "); \
	fprintf(stderr, ##message, ##__VA_ARGS__); \
	fprintf(stderr, "\n");\
	assert(false);\
}

#else

#define FlxError(message, ...) {\
	fprintf(stderr, __LOC__); \
	fprintf(stderr, "\nFlxError: "); \
	fprintf(stderr, ##message, ##__VA_ARGS__); \
	fprintf(stderr, "\n");\
	getchar();\
	exit(EXIT_FAILURE);\
}

#endif