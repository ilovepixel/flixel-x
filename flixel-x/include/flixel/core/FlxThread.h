#pragma once

#include <flixel/core/FlxPlatform.h>
#include <flixel/core/FlxTypes.h>
#include <thread>

#if defined(_MSC_VER)
#pragma warning(push)
#pragma warning(disable: 4251)
#endif

class FLX_API FlxThread
{
public:
	template<class Function>
	FlxThread(Function func) :
		stdThread(func)
	{}
	FlxThread(FlxThread&& move);
	FlxThread(std::thread&& stdThreadMove);
	virtual ~FlxThread();
	std::thread::id getThreadID();

	FLX_FORCEINLINE static void sleep(uint32 millisconds)
	{
		std::this_thread::sleep_for(std::chrono::milliseconds(millisconds));
	}

protected:
	std::thread stdThread;
};

#if defined(_MSC_VER)
#pragma warning(pop)
#endif