#pragma once

#include <flixel/core/FlxPlatform.h>
#include <immintrin.h>
#include <string.h>

// TODO: Implement custom allocator.
template<class T, size_t Alignment = 4>
class FlxArray
{
public:
	FlxArray(size_t capacity) :
		capacity(capacity),
		length(0),
		rawArray((T*)_mm_malloc(sizeof(T) * capacity, Alignment))
	{}
	FlxArray(FlxArray<T>& other) :
		capacity(other.capacity),
		length(other.length),
		rawArray((T*)_mm_malloc(sizeof(T) * other.capacity, Alignment))
	{
		memmove(rawArray, other.rawArray, sizeof(T) * other.capacity);
	}
	FlxArray() :
		capacity(1),
		length(0),
		rawArray((T*)_mm_malloc(sizeof(T) * capacity, Alignment))
	{}
	virtual ~FlxArray()
	{
		for (size_t index = 0; index < length; ++index)
		{
			rawArray[index].~T();
		}
		_mm_free(rawArray);
	}
	FLX_FORCEINLINE void clearAndDestroy()
	{
		for (size_t index = 0; index < length; ++index)
		{
			rawArray[index].~T();
		}
		length = 0;
	}
	FLX_FORCEINLINE void clear()
	{
		length = 0;
	}
	FLX_FORCEINLINE void push(T& element)
	{
		if (length >= capacity)
		{
			void* m = _mm_malloc(sizeof(T) * (capacity * 2), Alignment);
			m = memmove(m, rawArray, capacity * sizeof(T));
			_mm_free(rawArray);
			rawArray = (T*)m;
			capacity *= 2;
		}
		rawArray[length++] = element;
	}
	FLX_FORCEINLINE void push(T&& element)
	{
		if (length >= capacity)
		{
			void* m = _mm_malloc(sizeof(T) * (capacity * 2), Alignment);
			m = memmove(m, rawArray, capacity * sizeof(T));
			_mm_free(rawArray);
			rawArray = (T*)m;
			capacity *= 2;
		}
		rawArray[length++] = element;
	}
	FLX_FORCEINLINE T& pop()
	{
		return rawArray[--length];
	}
	FLX_FORCEINLINE size_t getLength()
	{
		return length;
	}
	FLX_FORCEINLINE size_t getByteLength()
	{
		return length * sizeof(T);
	}
	FLX_FORCEINLINE size_t getCapacity()
	{
		return capacity;
	}
	FLX_FORCEINLINE T& operator[](size_t index)
	{
		return rawArray[index];
	}
	FLX_FORCEINLINE T& get(size_t index)
	{
		return rawArray[index];
	}
	FLX_FORCEINLINE void reserve(size_t count)
	{
		if (count > capacity)
		{
			void* m = _mm_malloc(sizeof(T) * (count), Alignment);
			m = memmove(m, rawArray, capacity * sizeof(T));
			_mm_free(rawArray);
			rawArray = (T*)m;
			capacity = count;
		}
	}
	FLX_FORCEINLINE virtual void fill(T&& value)
	{
		for (size_t index = 0; index < length; ++index)
		{
			rawArray[index] = value;
		}
	}
	FLX_FORCEINLINE bool contains(T&& value)
	{
		for (size_t index = 0; index < length; ++index)
		{
			if (rawArray[index] == value)
			{
				return true;
			}
		}
		return false;
	}
	FLX_FORCEINLINE bool contains(T& value)
	{
		for (size_t index = 0; index < length; ++index)
		{
			if (rawArray[index] == value)
			{
				return true;
			}
		}
		return false;
	}
	FLX_FORCEINLINE int32 indexOf(T&& value)
	{
		for (int32 index = 0; index < (int32)length; ++index)
		{
			if (rawArray[index] == value)
			{
				return index;
			}
		}
		return -1;
	}
	FLX_FORCEINLINE int32 indexOf(T& value)
	{
		for (int32 index = 0; index < (int32)length; ++index)
		{
			if (rawArray[index] == value)
			{
				return index;
			}
		}
		return -1;
	}
protected:
	size_t capacity;
	size_t length;
	T* rawArray;
};