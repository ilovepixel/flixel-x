#pragma once

#include <flixel/core/FlxPlatform.h>
#include <flixel/core/FlxTypes.h>
#include <flixel/core/FlxAL.h>
#include <flixel/core/FlxWAV.h>

struct FLX_API FlxAudio
{
	enum FlxAudioLoadState
	{
		NOT_LOADED,
		LOADED,
		IN_PROGRESS,
		FAILED
	};
	FlxAudio();
	bool isLoaded();
	void setPitch(float32 pitch);
	void setGain(float32 gain);
	void setLoop(bool loop);
	float32 getPitch();
	float32 getGain();
	bool getLoop();

	ALuint bufferId;
	ALuint sourceId;

	static FlxAudio loadAudio(const char* path, bool loop = false, float32 pitch = 1.0f, float32 gain = 1.0f);
	static void discardAudio(FlxAudio audio);

private:
	FlxAudioLoadState state;
	bool loop;
	float32 gain;
	float32 pitch;
	FlxWaveData* waveData;
};