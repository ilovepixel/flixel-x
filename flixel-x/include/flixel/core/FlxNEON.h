#pragma once

#include <flixel/core/FlxPlatform.h>
#include <flixel/core/FlxTypes.h>

#include <arm_neon.h>

typedef float32x4_t FlxVectorReg;
namespace FlxSIMD
{
	FLX_API FLX_FORCEINLINE FlxVectorReg set(float32 x, float32 y, float32 z, float32 w)
	{
		const float32_t ptr[4] = { w, z, y, x };
		FlxVectorReg v = vld1q_f32(ptr);
		return v;
	}
	FLX_API FLX_FORCEINLINE FlxVectorReg setReverse(float32 x, float32 y, float32 z, float32 w)
	{
		const float32_t ptr[4] = { x, y, z, w };
		FlxVectorReg v = vld1q_f32(ptr);
		return v;
	}
	FLX_API FLX_FORCEINLINE FlxVectorReg setZero()
	{
		return vdupq_n_f32(0);
	}
	FLX_API FLX_FORCEINLINE FlxVectorReg setOne(float32 x)
	{
		return vdupq_n_f32(x);
	}
	FLX_API FLX_FORCEINLINE FlxVectorReg load(float32* scalar_vector)
	{
		return vld1q_f32(scalar_vector);
	}
	FLX_API FLX_FORCEINLINE FlxVectorReg loadUnaligned(float32* scalar_vector)
	{
		return vld1q_f32(scalar_vector);
	}
	FLX_API FLX_FORCEINLINE void store(float32* scalar_vector, FlxVectorReg dest)
	{
		vst1q_f32(scalar_vector, dest);
	}
	FLX_API FLX_FORCEINLINE void storeUnaligned(float32* scalar_vector, FlxVectorReg dest)
	{
		vst1q_f32(scalar_vector, dest);
	}
	FLX_API FLX_FORCEINLINE FlxVectorReg add(FlxVectorReg a, FlxVectorReg b)
	{
		return vaddq_f32(a, b);
	}
	FLX_API FLX_FORCEINLINE FlxVectorReg sub(FlxVectorReg a, FlxVectorReg b)
	{
		return vsubq_f32(a, b);
	}
	FLX_API FLX_FORCEINLINE FlxVectorReg mul(FlxVectorReg a, FlxVectorReg b)
	{
		return vmulq_f32(a, b);
	}
	FLX_API FLX_FORCEINLINE FlxVectorReg min(FlxVectorReg a, FlxVectorReg b)
	{
		return vminq_f32(a, b);
	}
	FLX_API FLX_FORCEINLINE FlxVectorReg max(FlxVectorReg a, FlxVectorReg b)
	{
		return vmaxq_f32(a, b);
	}
	FLX_API FLX_FORCEINLINE FlxVectorReg and(FlxVectorReg a, FlxVectorReg b)
	{
		return (FlxVectorReg)vandq_s32((int32x4_t)a, (int32x4_t)b);
	}
	FLX_API FLX_FORCEINLINE FlxVectorReg not(FlxVectorReg a, FlxVectorReg b)
	{
		return (FlxVectorReg)vbicq_s32((int32x4_t)a, (int32x4_t)b);
	}
	FLX_API FLX_FORCEINLINE FlxVectorReg or(FlxVectorReg a, FlxVectorReg b)
	{
		return (FlxVectorReg)vorrq_s32((int32x4_t)a, (int32x4_t)b);
	}
	FLX_API FLX_FORCEINLINE FlxVectorReg xor(FlxVectorReg a, FlxVectorReg b)
	{
		return (FlxVectorReg)veorq_s32((int32x4_t)a, (int32x4_t)b);
	}
	FLX_API FLX_FORCEINLINE FlxVectorReg equal(FlxVectorReg a, FlxVectorReg b)
	{
		return (FlxVectorReg)vceqq_f32(a, b);
	}
	FLX_API FLX_FORCEINLINE FlxVectorReg lowerThan(FlxVectorReg a, FlxVectorReg b)
	{
		return (FlxVectorReg)vcltq_f32(a, b);
	}
	FLX_API FLX_FORCEINLINE FlxVectorReg lowerThanOrEqual(FlxVectorReg a, FlxVectorReg b)
	{
		return (FlxVectorReg)vcleq_f32(a, b);
	}
	FLX_API FLX_FORCEINLINE FlxVectorReg greaterThan(FlxVectorReg a, FlxVectorReg b)
	{
		return (FlxVectorReg)vcgtq_f32(a, b);
	}
	FLX_API FLX_FORCEINLINE FlxVectorReg greaterThanOrEqual(FlxVectorReg a, FlxVectorReg b)
	{
		return (FlxVectorReg)vcgeq_f32(a, b);
	}
}