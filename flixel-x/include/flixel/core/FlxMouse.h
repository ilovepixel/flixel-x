#pragma once

#include <flixel/core/FlxPlatform.h>
#include <flixel/core/FlxTypes.h>
#include <flixel/core/FlxImage.h>

class FLX_API FlxMouse
{
public:
	FlxMouse();
	void reset();
	void setMousePos(float32 x, float32 y);
	void setMouseButtonDown(uint32 button);
	void setMouseButtonUp(uint32 button);
	bool isButtonDown(uint32 button);
	bool isButtonHit(uint32 button);
	bool isButtonUp(uint32 button);
	void draw();

	float32 x;
	float32 y;
	FlxImage image;

	/* GLFW Mouse Buttons */
	static constexpr uint32 MOUSE_BUTTON_1 = 0;
	static constexpr uint32 MOUSE_BUTTON_2 = 1;
	static constexpr uint32 MOUSE_BUTTON_3 = 2;
	static constexpr uint32 MOUSE_BUTTON_4 = 3;
	static constexpr uint32 MOUSE_BUTTON_5 = 4;
	static constexpr uint32 MOUSE_BUTTON_6 = 5;
	static constexpr uint32 MOUSE_BUTTON_7 = 6;
	static constexpr uint32 MOUSE_BUTTON_8 = 7;
	static constexpr uint32 MOUSE_BUTTON_LAST = MOUSE_BUTTON_8;
	static constexpr uint32 MOUSE_BUTTON_LEFT = MOUSE_BUTTON_1;
	static constexpr uint32 MOUSE_BUTTON_RIGHT = MOUSE_BUTTON_2;
	static constexpr uint32 MOUSE_BUTTON_MIDDLE = MOUSE_BUTTON_3;
	static constexpr size_t MAX_BUTTONS = 10;
private:
	bool mouseButtonsDown[MAX_BUTTONS];
	bool mouseButtonsHit[MAX_BUTTONS];
	bool mouseButtonsUp[MAX_BUTTONS];
};