#pragma once

#include <flixel/core/FlxPlatform.h>
#include <flixel/core/FlxTypes.h>
#include <flixel/core/FlxThread.h>
#include <flixel/core/FlxStaticArray.h>
#include <mutex>


#if defined(_MSC_VER)
#pragma warning(push)
#pragma warning(disable: 4251)
#endif

class FLX_API FlxWorker
{
public:
	static constexpr size_t MAX_JOBS = 100;
	typedef std::function<void(void)> FlxJobType;

	FlxWorker();
	virtual ~FlxWorker();
	void runJob(FlxJobType job);
	void wait();
	void stop();
	std::thread::id getThreadID();

protected:
	void work();
	
private:
	FlxStaticArray<FlxJobType, MAX_JOBS> jobs;
	std::mutex jobMutex;
	bool runWorker;
	FlxThread thread;
};

#if defined(_MSC_VER)
#pragma warning(pop)
#endif

/*
class LD_API ldWorker
{
public:
	static constexpr size_t MAX_JOBS = 100;
	typedef std::function<void(void)> JobType;

	ldWorker();
	virtual ~ldWorker();
	void runJob(JobType job);
	void wait();
	void sleep(uint32 milliseconds);
	void stopWorker();
	LD_INLINE std::thread::id getId() { return ldThread.threadId(); }

private:
	void work();
	//Implement a queue
	JobType jobQueue[MAX_JOBS];
	std::atomic<uint32> jobAtomicCounter;
	bool runWorker;
	uint32 sleepTime;
	ldThread ldThread;

};
#if defined(LD_MSVC)
#pragma warning(pop)
#endif
*/