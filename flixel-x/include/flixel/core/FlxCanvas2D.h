#pragma once

#include <flixel/core/FlxPlatform.h>
#include <flixel/core/FlxTypes.h>
#include <flixel/core/FlxImage.h>
#include <flixel/FlxPoint.h>
#include <flixel/core/FlxGL.h>
#include <flixel/core/FlxShader.h>
#include <flixel/core/FlxMat44.h>
#include <flixel/core/FlxStaticArray.h>

static constexpr size_t FLX_MAX_SPRITES = 2000;
static constexpr size_t FLX_VERTICES_PER_SPRITE = 6;
static constexpr size_t FLX_MAX_VERTEICES = FLX_MAX_SPRITES * FLX_VERTICES_PER_SPRITE;
static constexpr size_t FLX_MAX_MATRICES = 1000;

template class FLX_API FlxStaticArray<float32, FLX_MAX_VERTEICES * 2, 16>;
template class FLX_API FlxStaticArray<float32, FLX_MAX_VERTEICES * 2>;
template class FLX_API FlxStaticArray<float32, FLX_MAX_VERTEICES * 4>;
template class FLX_API FlxStaticArray<FlxMat44, FLX_MAX_MATRICES, 16>;

class FLX_API FlxCanvas2D
{
public:
	enum FlxDrawMode
	{
		NONE,
		PRIMITIVE,
		TEXTURE
	};

public:
	FlxCanvas2D();
	virtual ~FlxCanvas2D();
	void init(float32 width = 640.0f, float32 height = 480.0f, const char* title = "Flixel Game", bool fullscreen = false);
	void pushMatrix();
	void popMatrix();
	void translate(float32 x, float32 y);
	void scale(float32 x, float32 y);
	void rotate(float32 t);
	void setColor(float32 r, float32 g, float32 b);
	void setColor(float32 r, float32 g, float32 b, float32 a);
	void setAlpha(float32 a);
	void scissor(float32 x, float32 y, float32 width, float32 height);
	void drawLine(float32 x1, float32 y1, float32 x2, float32 y2);
	void drawCircle(float32 x, float32 y, float32 radius);
	void drawRect(float32 x, float32 y, float32 width, float32 height);
	void drawRectBorder(float32 x, float32 y, float32 width, float32 height);
	void drawImage(FlxImage image, float32 x, float32 y);
	void drawImageRect(FlxImage image, float32 x, float32 y, float32 rx, float32 ry, float32 rw, float32 rh);
	void clearScreen(float32 r, float32 g, float32 b);
	void flush();
	void flipBuffer();
	float32 getWidth();
	float32 getHeight();

private:
	void pushBatch(GLsizei vertexCount, GLenum drawModeGL, FlxDrawMode drawMode, FlxImage texture = FlxImage());

protected:
	float32 width;
	float32 height;

private:
	SDL_Window* sdlWindow;
	SDL_GLContext sdlGLContext;
	FlxShader primitiveShader;
	FlxShader textureShader;
	GLuint vertexPositionVBO;
	GLuint vertexColorVBO;
	GLuint texCoordsVBO;
	FlxStaticArray<float32, FLX_MAX_VERTEICES * 2, 16> vertexPositionData;
	FlxStaticArray<float32, FLX_MAX_VERTEICES * 4> vertexColorData;
	FlxStaticArray<float32, FLX_MAX_VERTEICES * 2> texCoordsData;
	GLsizei spriteCount;
	FlxStaticArray<FlxMat44, FLX_MAX_MATRICES, 16> matrixStack;
	FlxMat44 currentMatrix;
	float32 color[4];
	GLsizei currentVertexCount;
	FlxDrawMode currentDrawMode;
	GLenum currentGLDrawMode;
	FlxImage currentTexture;
};