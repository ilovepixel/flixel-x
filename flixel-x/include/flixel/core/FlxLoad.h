#pragma once

#include <flixel/core/FlxPlatform.h>
#include <flixel/core/FlxTypes.h>

struct FLX_API FlxLoad
{
	static void* loadFile(const char* path, size_t* outFileSize);
	static void destroyFile(void* fileData);
};