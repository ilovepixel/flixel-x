#pragma once

#if defined(FLX_EXPORT)
#define FLX_API __declspec(dllexport)
#else
#define FLX_API __declspec(dllimport)
#endif

#define FLX_FORCEINLINE __forceinline
#define FLX_NOINLINE __declspec(noinline)
#define FLX_ALIGN(X) __declspec(align(X))
#if _WIN32
#define _CRT_SECURE_NO_WARNINGS 1
#endif

template<class T0, class T1>
T0* FlxCast(T1* p)
{
	return dynamic_cast<T0*>(p);
}
