#pragma once

#include <flixel/core/FlxSIMD.h>
#include <flixel/core/FlxPlatform.h>
#include <flixel/core/FlxTypes.h>

struct FLX_API FlxVector4SIMD
{
	FlxVector4SIMD(float32 x = 0.0f, float32 y = 0.0f, float32 z = 0.0f, float32 w = 0.0f) :
		vectorData(FlxSIMD::setReverse(x, y, z, w))
	{}
	FlxVector4SIMD(FlxVectorReg&& other) :
		vectorData(other)
	{}
	static FlxVector4SIMD add(FlxVector4SIMD v1, FlxVector4SIMD v2)
	{
		return FlxVector4SIMD(FlxSIMD::add(v1.vectorData, v2.vectorData));
	}

	union FLX_ALIGN(16)
	{
		FlxVectorReg vectorData;
		struct
		{
			float32 x;
			float32 y;
			float32 z;
			float32 w;
		};
	};
};

struct FLX_API FlxMatrix4SIMD
{
	FlxMatrix4SIMD(float32 a = 1.0f, float32 b = 0.0f, float32 c = 0.0f, float32 d = 0.0f,
		float32 e = 0.0f, float32 f = 1.0f, float32 g = 0.0f, float32 h = 0.0f,
		float32 i = 0.0f, float32 j = 0.0f, float32 k = 1.0f, float32 l = 0.0f,
		float32 m = 0.0f, float32 n = 0.0f, float32 o = 0.0f, float32 p = 1.0f)
	{
		col0 = FlxSIMD::set(m, i, e, a);
		col1 = FlxSIMD::set(n, j, f, b);
		col2 = FlxSIMD::set(o, k, g, c);
		col3 = FlxSIMD::set(p, l, h, d);
	}
	FlxMatrix4SIMD(float32* pArray)
	{
		col0 = FlxSIMD::load(&pArray[0]);
		col1 = FlxSIMD::load(&pArray[1]);
		col2 = FlxSIMD::load(&pArray[2]);
		col3 = FlxSIMD::load(&pArray[3]);
	}
	FlxMatrix4SIMD(FlxMatrix4SIMD& copy)
	{
		col0 = copy.col0;
		col1 = copy.col1;
		col2 = copy.col2;
		col3 = copy.col3;
	}
	FlxMatrix4SIMD(FlxMatrix4SIMD&& move)
	{
		col0 = (move.col0);
		col1 = (move.col1);
		col2 = (move.col2);
		col3 = (move.col3);
	}
	void loadIdentity()
	{
		col0 = FlxSIMD::set(0, 0, 0, 1);
		col1 = FlxSIMD::set(0, 0, 1, 0);
		col2 = FlxSIMD::set(0, 1, 0, 0);
		col3 = FlxSIMD::set(1, 0, 0, 0);
	}
	bool operator==(FlxMatrix4SIMD& rhs)
	{
		return false;
	}
	FlxMatrix4SIMD& operator=(const FlxMatrix4SIMD& rhs)
	{
		col0 = rhs.col0;
		col1 = rhs.col1;
		col2 = rhs.col2;
		col3 = rhs.col3;
		return *this;
	}
	FlxMatrix4SIMD& operator=(FlxMatrix4SIMD&& rhs)
	{
		col0 = (rhs.col0);
		col1 = (rhs.col1);
		col2 = (rhs.col2);
		col3 = (rhs.col3);
		return *this;
	}
	union FLX_ALIGN(16)
	{
		struct
		{
			FlxVectorReg col0;
			FlxVectorReg col1;
			FlxVectorReg col2;
			FlxVectorReg col3;
		};
		float32 scalarCols[16];
	};
	static FlxVector4SIMD mulMat44Vec4(FlxMatrix4SIMD& lhs, FlxVector4SIMD& rhs)
	{
		FlxVector4SIMD ret;
#if defined(__ARM_NEON__)
		FlxVectorReg sMul0 = FlxSIMD::mul(lhs.col0, FlxSIMD::setOne(rhs.x));
		FlxVectorReg sMul1 = FlxSIMD::mul(lhs.col1, FlxSIMD::setOne(rhs.y));
		FlxVectorReg sMul2 = FlxSIMD::mul(lhs.col2, FlxSIMD::setOne(rhs.z));
		FlxVectorReg sMul3 = FlxSIMD::mul(lhs.col3, FlxSIMD::setOne(rhs.w));
#else
		FlxVectorReg sMul0 = FlxSIMD::mul(lhs.col0, _mm_shuffle_ps(rhs.vectorData, rhs.vectorData, _MM_SHUFFLE(0, 0, 0, 0)));
		FlxVectorReg sMul1 = FlxSIMD::mul(lhs.col1, _mm_shuffle_ps(rhs.vectorData, rhs.vectorData, _MM_SHUFFLE(1, 1, 1, 1)));
		FlxVectorReg sMul2 = FlxSIMD::mul(lhs.col2, _mm_shuffle_ps(rhs.vectorData, rhs.vectorData, _MM_SHUFFLE(2, 2, 2, 2)));
		FlxVectorReg sMul3 = FlxSIMD::mul(lhs.col3, _mm_shuffle_ps(rhs.vectorData, rhs.vectorData, _MM_SHUFFLE(3, 3, 3, 3)));
#endif
		FlxVectorReg sAdd0 = FlxSIMD::add(sMul0, sMul1);
		FlxVectorReg sAdd1 = FlxSIMD::add(sMul2, sMul3);
		FlxVectorReg result = FlxSIMD::add(sAdd0, sAdd1);
		ret.vectorData = result;
		return ret;
	}

	static FlxMatrix4SIMD mul(FlxMatrix4SIMD& lhs, FlxMatrix4SIMD& rhs)
	{
		FlxMatrix4SIMD ret;
#if defined(__ARM_NEON__)
		FlxVectorReg sMul0 = FlxSIMD::mul(lhs.col0, FlxSIMD::setOne(rhs.scalarCols[0]));
		FlxVectorReg sMul1 = FlxSIMD::mul(lhs.col1, FlxSIMD::setOne(rhs.scalarCols[1]));
		FlxVectorReg sMul2 = FlxSIMD::mul(lhs.col2, FlxSIMD::setOne(rhs.scalarCols[2]));
		FlxVectorReg sMul3 = FlxSIMD::mul(lhs.col3, FlxSIMD::setOne(rhs.scalarCols[3]));
#else
		FlxVectorReg sMul0 = FlxSIMD::mul(lhs.col0, _mm_shuffle_ps(rhs.col0, rhs.col0, _MM_SHUFFLE(0, 0, 0, 0)));
		FlxVectorReg sMul1 = FlxSIMD::mul(lhs.col1, _mm_shuffle_ps(rhs.col0, rhs.col0, _MM_SHUFFLE(1, 1, 1, 1)));
		FlxVectorReg sMul2 = FlxSIMD::mul(lhs.col2, _mm_shuffle_ps(rhs.col0, rhs.col0, _MM_SHUFFLE(2, 2, 2, 2)));
		FlxVectorReg sMul3 = FlxSIMD::mul(lhs.col3, _mm_shuffle_ps(rhs.col0, rhs.col0, _MM_SHUFFLE(3, 3, 3, 3)));
#endif
		FlxVectorReg sAdd0 = FlxSIMD::add(sMul0, sMul1);
		FlxVectorReg sAdd1 = FlxSIMD::add(sMul2, sMul3);
		FlxVectorReg result = FlxSIMD::add(sAdd0, sAdd1);
		ret.col0 = result;

#if defined(__ARM_NEON__)
		sMul0 = FlxSIMD::mul(lhs.col0, FlxSIMD::setOne(rhs.scalarCols[4]));
		sMul1 = FlxSIMD::mul(lhs.col1, FlxSIMD::setOne(rhs.scalarCols[5]));
		sMul2 = FlxSIMD::mul(lhs.col2, FlxSIMD::setOne(rhs.scalarCols[6]));
		sMul3 = FlxSIMD::mul(lhs.col3, FlxSIMD::setOne(rhs.scalarCols[7]));
#else
		sMul0 = FlxSIMD::mul(lhs.col0, _mm_shuffle_ps(rhs.col1, rhs.col1, _MM_SHUFFLE(0, 0, 0, 0)));
		sMul1 = FlxSIMD::mul(lhs.col1, _mm_shuffle_ps(rhs.col1, rhs.col1, _MM_SHUFFLE(1, 1, 1, 1)));
		sMul2 = FlxSIMD::mul(lhs.col2, _mm_shuffle_ps(rhs.col1, rhs.col1, _MM_SHUFFLE(2, 2, 2, 2)));
		sMul3 = FlxSIMD::mul(lhs.col3, _mm_shuffle_ps(rhs.col1, rhs.col1, _MM_SHUFFLE(3, 3, 3, 3)));
#endif
		sAdd0 = FlxSIMD::add(sMul0, sMul1);
		sAdd1 = FlxSIMD::add(sMul2, sMul3);
		result = FlxSIMD::add(sAdd0, sAdd1);
		ret.col1 = result;

#if defined(__ARM_NEON__)
		sMul0 = FlxSIMD::mul(lhs.col0, FlxSIMD::setOne(rhs.scalarCols[8]));
		sMul1 = FlxSIMD::mul(lhs.col1, FlxSIMD::setOne(rhs.scalarCols[9]));
		sMul2 = FlxSIMD::mul(lhs.col2, FlxSIMD::setOne(rhs.scalarCols[10]));
		sMul3 = FlxSIMD::mul(lhs.col3, FlxSIMD::setOne(rhs.scalarCols[11]));
#else
		sMul0 = FlxSIMD::mul(lhs.col0, _mm_shuffle_ps(rhs.col2, rhs.col2, _MM_SHUFFLE(0, 0, 0, 0)));
		sMul1 = FlxSIMD::mul(lhs.col1, _mm_shuffle_ps(rhs.col2, rhs.col2, _MM_SHUFFLE(1, 1, 1, 1)));
		sMul2 = FlxSIMD::mul(lhs.col2, _mm_shuffle_ps(rhs.col2, rhs.col2, _MM_SHUFFLE(2, 2, 2, 2)));
		sMul3 = FlxSIMD::mul(lhs.col3, _mm_shuffle_ps(rhs.col2, rhs.col2, _MM_SHUFFLE(3, 3, 3, 3)));
#endif
		sAdd0 = FlxSIMD::add(sMul0, sMul1);
		sAdd1 = FlxSIMD::add(sMul2, sMul3);
		result = FlxSIMD::add(sAdd0, sAdd1);
		ret.col2 = result;

#if defined(__ARM_NEON__)
		sMul0 = FlxSIMD::mul(lhs.col0, FlxSIMD::setOne(rhs.scalarCols[12]));
		sMul1 = FlxSIMD::mul(lhs.col1, FlxSIMD::setOne(rhs.scalarCols[13]));
		sMul2 = FlxSIMD::mul(lhs.col2, FlxSIMD::setOne(rhs.scalarCols[14]));
		sMul3 = FlxSIMD::mul(lhs.col3, FlxSIMD::setOne(rhs.scalarCols[15]));
#else
		sMul0 = FlxSIMD::mul(lhs.col0, _mm_shuffle_ps(rhs.col3, rhs.col3, _MM_SHUFFLE(0, 0, 0, 0)));
		sMul1 = FlxSIMD::mul(lhs.col1, _mm_shuffle_ps(rhs.col3, rhs.col3, _MM_SHUFFLE(1, 1, 1, 1)));
		sMul2 = FlxSIMD::mul(lhs.col2, _mm_shuffle_ps(rhs.col3, rhs.col3, _MM_SHUFFLE(2, 2, 2, 2)));
		sMul3 = FlxSIMD::mul(lhs.col3, _mm_shuffle_ps(rhs.col3, rhs.col3, _MM_SHUFFLE(3, 3, 3, 3)));
#endif
		sAdd0 = FlxSIMD::add(sMul0, sMul1);
		sAdd1 = FlxSIMD::add(sMul2, sMul3);
		result = FlxSIMD::add(sAdd0, sAdd1);
		ret.col3 = result;
		return ret;
	}
};
