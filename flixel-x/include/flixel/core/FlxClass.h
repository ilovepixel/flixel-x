#pragma once

#include <flixel/core/FlxPlatform.h>


struct FLX_API FlxClass 
{
	typedef std::function<FlxClass*(void)> Type;
};

template<class T>
FlxClass::Type GetClass(T* x = nullptr)
{
	return []() -> FlxClass* { return new T(); };
}


template class FLX_API std::function<FlxClass*(void)>;
