#pragma once

#include <flixel/core/FlxPlatform.h>
#include <flixel/core/FlxTypes.h>
#include <flixel/core/FlxAudio.h>

class FLX_API FlxAudioPlayer
{
public:
	FlxAudioPlayer();
	virtual ~FlxAudioPlayer();
	void init();
	void play(FlxAudio audio);
	void stop(FlxAudio audio);

private:
	ALCdevice* device;
	ALCcontext* context;
};