#pragma once

#include <flixel/core/FlxPlatform.h>
#include <flixel/core/FlxTypes.h>

struct FLX_API FlxWaveData
{
	static FlxWaveData* loadWAV(const char* path);
	static void destroyWAV(FlxWaveData* waveData);
	static FlxWaveData* parseWAV(void* rawData);

	uint8 chunkId[4];
	uint32 chunkSize;
	uint8 format[4];
	uint8 subchunk1Id[4];
	uint32 subchunk1Size;
	uint16 audioFormat;
	uint16 numChannels;
	uint32 sampleRate;
	uint32 byteRate;
	uint16 blockAlign;
	uint16 bitsPerSample;
	uint8 subchunk2Id[4];
	uint32 subchunk2Size;
	void* data;
};

