#pragma once

#include <flixel/core/FlxPlatform.h>
#include <flixel/core/FlxTypes.h>
#include <flixel/core/FlxGL.h>

class FLX_API FlxShader
{
public:
	FlxShader();
	virtual ~FlxShader();
	void build(const char* vertShaderSource, const char* fragShaderSource);
	void enable();
	void disable();
	void discard();
	GLuint getAttribLocation(const char* name);
	GLuint getUniformLocation(const char* name);

private:
	GLuint vertexShader;
	GLuint fragmentShader;
	GLuint program;
};