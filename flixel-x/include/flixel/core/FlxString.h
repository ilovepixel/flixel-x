#pragma once

#include <flixel/core/FlxTypes.h>
#include <flixel/core/FlxArray.h>

class FLX_API FlxString : public FlxArray<char>
{
public:
	FlxString(char* charArray);
	FlxString(FlxString& string);
	FlxString& operator=(char* charArray);
	//FlxString& operator=(FlxString& string);
	FlxString& operator+=(char* charArray);
	FlxString& operator+=(FlxString& string);
	bool operator==(char* charArray);
	bool operator==(FlxString& string);
	int32 hash(size_t max);
	virtual void fill(char&& value) override;
	char* getChars();
};