#pragma once

#include <flixel/core/FlxCore.h>
#include <flixel/FlxBasic.h>

class FlxObject;
class FLX_API FlxSound : public FlxBasic
{
public:
	FlxSound();
	virtual ~FlxSound();
	virtual void destroy() override;
	virtual void update() override;
	virtual void kill() override;
	void createSound();
	void loadAudio(FlxAudio audio, bool looped = false, bool autoDestroy = false);
	FlxSound& proximity(float32 x, float32 y, FlxObject* object, float32 radius, bool pan = true);
	void play(bool forceRestart = false);
	void resume();
	void stop();
	void pause();
	void fadeOut(float32 seconds, bool pauseInstead = false);
	void fadeIn(float32 seconds);
	void updateTransform();
	float32 getVolume();
	void setVolume(float32 volume);
	float32 getActualvolume();
	
public:
	float32 x;
	float32 y;
	bool survive;
	float32 amplitude;
	float32 amplitudeLeft;
	float32 amplitudeRight;
	bool autoDestroy;

private:
	FlxAudio _sound;
	float32 _volume;
	float32 _volumeAdjust;
	bool _looped;
	FlxObject* _target;
	float32 _radius;
	bool _pan;
	float32 _position;
	float32 _fadeOutTimer;
	float32 _fadeOutTotal;
	bool _pauseOnFadeOut;
	float32 _fadeInTimer;
	float32 _fadeInTotal;

};