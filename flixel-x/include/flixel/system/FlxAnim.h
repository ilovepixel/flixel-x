#pragma once

#include <flixel/core/FlxCore.h>

template class FLX_API FlxArray<uint32>;

struct FLX_API FlxAnim
{
	FlxAnim(char* name, FlxArray<uint32> frames, float32 frameRate = 0, bool looped = true);
	FlxAnim(FlxAnim& anim);
	void destroy();
	virtual ~FlxAnim();
	bool operator==(FlxAnim& other);
	FlxString name;
	float32 delay;
	FlxArray<uint32> frames;
	bool looped;
};