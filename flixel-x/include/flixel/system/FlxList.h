#pragma once

#include <flixel/core/FlxPlatform.h>
#include <flixel/core/FlxTypes.h>
#include <flixel/FlxObject.h>

struct FLX_API FlxList
{
	FlxList();
	void destroy();
	FlxObject* object;
	FlxList* next;
};