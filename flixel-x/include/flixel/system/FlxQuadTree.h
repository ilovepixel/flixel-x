#pragma once

#include <flixel/FlxRect.h>
#include <flixel/FlxObject.h>
#include <flixel/FlxObject.h>
#include <flixel/system/FlxList.h>

template class FLX_API std::function<bool(FlxObject*, FlxObject*)>;

class FLX_API FlxQuadTree : public FlxRect
{
public:
	typedef std::function<bool(FlxObject*, FlxObject*)> FlxCallback;
	FlxQuadTree(float32 x, float32 y, float32 width, float32 height, FlxQuadTree* parent = nullptr);
	void destroy();
	void load(FlxBasic* objectOrGroup1, FlxBasic* objectOrGroup2, FlxCallback notifyCallback = nullptr, FlxCallback processCallback = nullptr);
	void add(FlxBasic* objectOrGroup, uint32 list);
	void addObject();
	void addToList();
	bool execute();
	bool overlapNode();

public:
	static const uint32 A_LIST = 0;
	static const uint32 B_LIST = 1;
	static uint32 divisions;

private:
	static uint32 _min;
	static FlxObject* _object;
	static float32 _objectLeftEdge;
	static float32 _objectRightEdge;
	static float32 _objectTopEdge;
	static float32 _objectBottomEdge;
	static uint32 _list;
	static bool _useBothList;
	static FlxList* _iterator;
	static float32 _objectHullX;
	static float32 _objectHullY;
	static float32 _objectHullWidth;
	static float32 _objectHullHeight;
	static float32 _checkObjectHullX;
	static float32 _checkObjectHullY;
	static float32 _checkObjectHullWidth;
	static float32 _checkObjectHullHeight;
	static FlxCallback _processingCallback;
	static FlxCallback _notifyCallback;


	bool _canSubdivide;
	FlxList* _headA;
	FlxList* _tailA;
	FlxList* _headB;
	FlxList* _tailB;
	FlxQuadTree* _northWestTree;
	FlxQuadTree* _northEastTree;
	FlxQuadTree* _southWestTree;
	FlxQuadTree* _southEastTree;
	float32 _leftEdge;
	float32 _rightEdge;
	float32 _topEdge;
	float32 _bottomEdge;
	float32 _halfWidth;
	float32 _halfHeight;
	float32 _midpointX;
	float32 _midpointY;
};