#pragma once

#include <flixel/core/FlxCore.h>
#include <flixel/FlxBasic.h>
#include <flixel/FlxRect.h>
#include <flixel/FlxPoint.h>
#include <flixel/FlxU.h>
#include <flixel/core/FlxCanvas2D.h>

template class FLX_API FlxArray<uint8>;
class FlxObject;
class FLX_API FlxCamera : public FlxBasic
{
public:
	static FlxCamera* Create(float32 x, float32 y, float32 width, float32 height, float32 zoom = 0)
	{
		return new FlxCamera(x, y, width, height, zoom);
	}
	static void Destroy(FlxCamera* object) 
	{
		object->destroy();
		delete object;
	}
	FlxCamera(float32 x, float32 y, float32 width, float32 height, float32 zoom = 0);
	virtual void destroy() override;
	virtual void update() override;
	void follow(FlxObject* target, uint32 style = 0);
	void focusOn(FlxPoint& point);
	void setBounds(float32 x = 0, float32 y = 0, float32 width = 0, float32 height = 0, bool updateWorld = false);
	void flash(uint32 color = 0xFFFFFFFF, float32 duration = 1.0f, bool force = false);
	void fade(uint32 color = 0xFF000000, float32 duration = 1.0f, bool force = false);
	void shake(float32 intensity = 0.05f, float32 duration = 0.5f, bool force = false, uint32 direction = 0);
	void stopFX();
	FlxCamera& copyFrom(FlxCamera& camera);
	float32 getZoom();
	void setZoom(float32 zoom);
	float32 getAlpha();
	void setAlpha(float32 alpha);
	float32 getAngle();
	void setAngle(float32 angle);
	uint32 getColor();
	void setColor(uint32 color);
	bool isAntialiasing();
	void setAntialiasing(bool antialiasing);
	FlxPoint getScale();
	void setScale(float32 x, float32 y);
	void fill(uint32 color);
	void fillRGBA(float32 r, float32 g, float32 b, float32 a);
	void preDraw();
	void postDraw();

protected:
	void drawFX();

public:
	static const uint32 STYLE_LOCKON = 0;
	static const uint32 STYLE_PLATFORMER = 1;
	static const uint32 STYLE_TOPDOWN = 2;
	static const uint32 STYLE_TOPDOWN_TIGHT = 3;
	static const uint32 SHAKE_BOTH_AXES = 0;
	static const uint32 SHAKE_HORIZONTAL_ONLY = 1;
	static const uint32 SHAKE_VERTICAL_ONLY = 2;
	static float32 defaultZoom;

	float32 x;
	float32 y;
	float32 width;
	float32 height;
	FlxObject* target;
	FlxRect deadzone;
	FlxRect bounds;
	FlxPoint scroll;
	FlxCanvas2D* canvas;
	uint32 bgColor;

protected:
	float32 _angle;
	FlxPoint _scale;
	uint8 _colorArray[4];
	uint8 _tempColorArray[4];

	float32 _zoom;
	FlxPoint _point;
	uint32 _color;
	uint32 _fxFlashColor;
	float32 _fxFlashDuration;
	float32 _fxFlashAlpha;
	uint32 _fxFadeColor;
	float32 _fxFadeDuration;
	float32 _fxFadeAlpha;
	float32 _fxShakeIntensity;
	float32 _fxShakeDuration;
	FlxPoint _fxShakeOffset;
	uint32 _fxShakeDirection;
};

