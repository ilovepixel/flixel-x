#pragma once

#include <flixel/core/FlxCore.h>
#include <flixel/core/FlxMat44.h>

struct FLX_API FlxPoint
{
	static FlxPoint* Create(float32 x = 0, float32 y = 0)
	{
		return new FlxPoint(x, y);
	}
	static void Destroy(FlxPoint* object)
	{
		delete object;
	}
	FlxPoint(float32 x = 0, float32 y = 0);
	FlxPoint(FlxPoint& point);
	FlxPoint& make(float32 x, float32 y);
	FlxPoint& copyFrom(FlxPoint& other);
	FlxPoint& copyTo(FlxPoint& other);
	
	FLX_FORCEINLINE FlxVec4 toVec4()
	{
		return FlxVec4(x, y, 1.0, 1.0);
	}
	FLX_FORCEINLINE FlxPoint& fromVec4(FlxVec4& vec4) 
	{
		x = vec4.x;
		y = vec4.y;
		return *this;
	}

	float32 x;
	float32 y;
};
