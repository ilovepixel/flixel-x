#pragma once

#include <flixel/core/FlxCore.h>

class FlxCamera;
static constexpr size_t FLX_MAX_CAMERAS = 100;
template class FLX_API FlxStaticArray<FlxCamera*, FLX_MAX_CAMERAS>;

class FLX_API FlxBasic
{
public:
	static uint32 _ACTIVECOUNT;
	static uint32 _VISIBLECOUNT;

public:
	enum FlxBasicType
	{
		FLXBASIC,
		FLXGROUP,
		FLXTILEMAP,
		FLXOBJECT
	};

	static FlxBasic* Create()
	{
		return new FlxBasic();
	}
	static void Destroy(FlxBasic* object)
	{
		object->destroy();
		delete object;
	}
	FlxBasic();
	virtual void destroy() {}
	virtual void preUpdate();
	virtual void update() {}
	virtual void postUpdate() {}
	virtual void draw();
	virtual void drawDebug(FlxCamera* camera = nullptr) {}
	virtual void kill();
	virtual void revive();

	virtual FlxBasicType getType();

	int32 ID;
	bool exists;
	bool active;
	bool visible;
	bool alive;
	FlxStaticArray<FlxCamera*, FLX_MAX_CAMERAS>* cameras;
	bool ignoreDrawDebug;
};